/*
Navicat MySQL Data Transfer

Source Server         : Aisyah
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : aadc

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-09-08 17:11:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_about`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_about`;
CREATE TABLE `tbl_about` (
  `id_about` int(11) NOT NULL AUTO_INCREMENT,
  `nama_about` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_about`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_about
-- ----------------------------
INSERT INTO `tbl_about` VALUES ('4', 'Tentang Bayan', '<p style=\"text-align:justify\">Bayan Group dibangun berdasarkan kekuatan pengalaman kami di Indonesia. Minat bisnis Direktur Utama dan Pendiri Bayan Group, Dato&rsquo; Dr. Low Tuck Kwong, bermula di Indonesia pada tahun 1973 saat beliau mendirikan PT. Jaya Sumpiles Indonesia (JSI) sebagai kontraktor pekerjaan tanah, pekerjaan umum, dan struktur kelautan. JSI dengan cepat menjadi perintis dalam pekerjaan fondasi tumpuk (pile foundation) yang kompleks dan menjadi kontraktor terkemuka di Indonesia di bidang-bidang di atas selama era 1980an dan 1990an. &nbsp;Pada tahun 1988, JSI merambah ke pertambangan batubara kontrak dan menjadi kontraktor tambang terkemuka hingga tahun 1998, saat Dato&rsquo; Dr. Low mengakuisisi PT. Gunungbayan Pratamacoal (GBP) dan PT. Dermaga Perkasapratama (DPP). Saat itu GBP belum memulai pertambangan dan Balikpapan Coal Terminal (di bawah DPP) memiliki kapasitas 2,5 juta ton per tahun.&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><br />\r\nDi bawah kepemimpinan Dato&rsquo; Dr. Low, Bayan Group dengan cepat bertransformasi menjadi perusahaan tambang batubara terintegrasi vertikal yang sukses dan bereputasi. Bayan Group dibentuk melalui sejumlah akuisisi strategis di sektor batubara dan didirikan dengan prestasi terbukti dalam mengembangkan tambang batubara baru (greenfield).&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><br />\r\nSecara khusus, di tahun-tahun terakhir Proyek Tabang/Pakar telah berekspansi dari operasional tambang skala kecil yang hanya memproduksi 1,9 juta ton pada tahun 2014 menjadi produsen sekitar 22,7 juta ton batubara pada tahun 2018. Hal ini telah secara pesat meningkatkan posisi Bayan di pasar batubara, menjadi 5 besar produsen batubara di Indonesia. Pertumbuhan lebih lanjut telah direncanakan untuk tahun-tahun berikutnya, dengan target untuk meningkatkan Proyek Tabang/Pakar agar memproduksi sekitar 50 juta ton per tahun.&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\"><br />\r\nBayan Group merupakan inovator di bidang industri pertambangan batubara dan terus menilik metode dan teknologi baru untuk mengukuhkan posisinya sebagai salah satu produsen batubara berbiaya terendah di Indonesia. Bayan Group misalnya adalah perusahaan tambang pertama di Indonesia yang memanfaatkan through-seam blasting di tambang Wahana, metode pertambangan dozer-push di tambang Tabang, dan saat ini menggunakan beberapa truk pengangkutan batubara terbesar di Indonesia dengan kapasitas 200-220 ton. Kami mengharapkan tren ini akan terus berlanjut selagi kami mengkaji berbagai peluang lain.</p>\r\n\r\n<p style=\"text-align:justify\">Bayan Group memiliki beberapa infrastruktur batubara terdepan di Indonesia dengan kepemilikan atas Balikpapan Coal Terminal, Dermaga Perkasa dan Wahana, serta dua Floating Transfer Barge (KFT). Fasilitas-fasilitas ini mencakup kemampuan untuk membongkar batubara, menimbun batubara, dan memuat kapal pada kecepatan antara 3.000 &ndash; 8.000 ton per jam. Beberapa fasilitas ini juga dapat mencampur batubara dari berbagai tempat penimbunan (stockpile) dan kualitas. Kami terus berinvestasi untuk memperluas fasilitas-fasilitas ini apabila diperlukan, memberikan fleksibilitas kepada pelanggan dalam hal kapal yang digunakan serta redundansi kapasitas kami.</p>\r\n');
INSERT INTO `tbl_about` VALUES ('5', 'Visi & Misi', '<h4 style=\"text-align:justify\">Visi</h4>\r\n\r\n<p style=\"text-align:justify\">Menjadi perusahaan pertambangan batubara yang disegani, yang berkomitmen untuk memberikan produk premium, layanan berkualitas tinggi, dan pertumbuhan berkesinambungan dalam jangka panjang sekaligus meminimalkan dampak lingkungan.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<h4 style=\"text-align:justify\">Misi</h4>\r\n\r\n<ol>\r\n	<li style=\"text-align:justify\">\r\n	<p>Mengoptimalkan nilai pemegang saham melalui pencapaian kinerja terbaik di semua operasi kami.</p>\r\n	</li>\r\n	<li style=\"text-align:justify\">\r\n	<p>Memaksimalkan kompetensi inti melalui pelaksanaan praktik bisnis terbaik.</p>\r\n	</li>\r\n	<li style=\"text-align:justify\">\r\n	<p>Menjunjung Tanggung Jawab Sosial Perusahaan dengan menitikberatkan peningkatan kesejahteraan karyawan, standar kesehatan dan keselamatan yang tinggi, kebijakan lingkungan yang berkesinambungan, dan pengembangan masyarakat yang bertanggung jawab.</p>\r\n	</li>\r\n</ol>\r\n');
INSERT INTO `tbl_about` VALUES ('6', 'Dari Presiden Direktur', '<p>&nbsp;</p>\r\n\r\n<table border=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:250px\"><img alt=\"\" src=\"https://www.bayan.com.sg/cfind/source/images/bod-1.png\" /></td>\r\n			<td>\r\n			<p><span style=\"font-size:18px\"><strong>Dato&rsquo; Dr. Low Tuck Kwong</strong></span><br />\r\n			<span style=\"font-size:16px\"><strong>President Director</strong></span></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\" style=\"background-color:#ffffff\">\r\n			<p>Para Pemangku Kepentingan yang Terhormat,&nbsp;</p>\r\n\r\n			<p style=\"text-align:justify\">Suatu kehormatan bagi saya dapat melihat Bayan Group bertumbuh dari sejumlah tambang yang relatif kecil hingga menjadi salah satu produsen batubara terbesar di Indonesia saat ini. Hal ini dapat tercapai melalui kerja keras dan dedikasi manajemen dan para karyawan, dan merupakan suatu kehormatan dapat bekerja sama dengan tim ini. Tentu saja perjuangan belum selesai, dan kami mempunyai visi untuk mengembangkan Bayan Group lebih lanjut di tahun-tahun mendatang dengan target keseluruhan lebih dari 50 juta ton per tahun.&nbsp;</p>\r\n\r\n			<p style=\"text-align:justify\">Proyek Tabang/Pakar masih dan akan tetap menjadi andalan Group kami untuk bertahun-tahun ke depan. Proyek ini memiliki cadangan signifikanbatubara sub-bituminus berkadar abu dan belerang rendah, yang memastikan usia tambang yang panjang yang didukung izin-izin konsesi. Produksi akan terus bertumbuh di tahun-tahun mendatang sementara Bayan Group telah dan akan terus berinvestasi pada infrastruktur penting yang dikhususkan untuk penggunaannya. Hal ini menghasilkan marjin laba tertinggi di industri ini untuk proyek ini, posisi yang kami harap dapat kami pertahankan melalui evaluasi dan implementasi teknologi terdepan.&nbsp;</p>\r\n\r\n			<p style=\"text-align:justify\">Kami berkomitmen untuk tetap menjadi produsen batubara thermal yang terutama beroperasi di Indonesia. Meskipun fokus utama kami adalah meningkatkan pertumbuhan secara organik dengan Proyek Tabang/Pakar, dengan arus kas yang solid, tingkat utang atau leverage rendah, dan infrastruktur termutakhir, kami dapat mempertimbangkan berbagai peluang yang mungkin tidak dapat dilakukan pelaku bisnis lain di sektor ini. &nbsp;Seperti yang telah kami lakukan di masa lalu, preferensi kami adalah mengembangkan peluang tambang baru (Greenfield) untuk produksi untuk memaksimalkan nilai pemegang saham. Kami terus mencari dan mengkaji berbagai peluang yang sejalan dengan tujuan kami.</p>\r\n\r\n			<p style=\"text-align:justify\">Lebih lanjut, saya ingin memberi penghargaan atas dukungan masyarakat di sekitar lokasi proyek-proyek kami. Saat ini kami adalah kontributor terbesar pengembangan masyarakat di Kutai Kartanegara, dan dengan pertumbuhan proyek Tabang/Pakar, kami berharap hal ini akan terus berlanjut. Kami telah membawa banyak pembangunan ke daerah-daerah sangat terpencil, yang tidak dapat menikmati pembangunan penting lain di sekitarnya. Kami akan terus mempekerjakan dan melatih orang-orang dari masyarakat sekitar serta berinvestasi dalam infrastruktur, pendidikan dan kesejahteraan masyarakat. &nbsp;&nbsp;</p>\r\n\r\n			<p style=\"text-align:justify\">Akhirnya, saya ingin memberikan penghargaan kepada para kontraktor, pemasok, pembiaya, pemegang saham dan para pemangku kepentingan kami yang lain. &nbsp;Diperlukan upaya tim untuk mengembangkan segala tambang, dan tanpa dukungan penuh semua pemangku kepentingan kami, semua ini tidak mungkin terwujud.&nbsp;</p>\r\n\r\n			<p>Kami percaya masa depan Bayan Group akan sangat cerah.&nbsp;</p>\r\n\r\n			<p>Terima kasih.&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n');
INSERT INTO `tbl_about` VALUES ('8', 'Sekretaris Perusahaan', '<p>&nbsp;</p>\r\n\r\n<table border=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:250px\"><img alt=\"\" src=\"https://www.bayan.com.sg/cfind/source/thumb/images/bod/cover_w175_h175_jenny.jpg\" /></td>\r\n			<td>\r\n			<p><span style=\"font-size:18px\"><strong>Jenny Quantero</strong></span><br />\r\n			<span style=\"font-size:16px\"><strong>Direktur Urusan Korporasi dan Sekretaris Perusahaan</strong></span></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\" style=\"background-color:#ffffff\">\r\n			<p style=\"text-align:justify\">Sekretaris Perusahaan bertindak sebagai penghubung antara Perusahaan dan Pemerintah atau instansi terkait, otoritas pasar modal, pemegang saham, pemangku kepentingan, dan media. Peran Sekretaris Perusahaan dijabat oleh Jenny Quantero berdasarkan surat</p>\r\n\r\n			<p style=\"text-align:justify\">Direksi tertanggal 18 Maret 2008 tentang penunjukan Sekretaris Perusahaan.</p>\r\n\r\n			<p style=\"text-align:justify\">Perusahaan memiliki kebijakan untuk tidak mengatur jangka waktu atau masa jabatan Sekretaris Perusahaan, namun Perseroan dapat mengganti Sekretaris Perusahaan sewaktu-waktu jika dipandang perlu oleh manajemen.&nbsp;</p>\r\n\r\n			<p style=\"text-align:justify\">Sekretaris Perusahaan mempunyai tugas dan tanggung jawab berikut:&nbsp;</p>\r\n\r\n			<ol>\r\n				<li style=\"text-align:justify\">Memastikan kepatuhan Perusahaan pada peraturan perundang-undangan yang berlaku.</li>\r\n				<li style=\"text-align:justify\">Memberikan pelayanan dan informasi yang dibutuhkan oleh para pemangku kepentingan dan pemegang saham.</li>\r\n				<li style=\"text-align:justify\">Membantu Direksi, Dewan Komisaris dan para komite dalam menyelenggarakan berbagai rapat, dari aspek administrasi hingga dokumentasi</li>\r\n				<li style=\"text-align:justify\">Membantu Perusahaan dalam menyelenggarakan berbagai kegiatan korporasi.</li>\r\n			</ol>\r\n\r\n			<p style=\"text-align:justify\"><strong><em>Hubungi Kami</em></strong></p>\r\n\r\n			<p style=\"text-align:justify\">PT Bayan Resources Tbk (BYAN) berdomisili di :</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table border=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Nama</td>\r\n			<td>: PT BAYAN RESOURCES TBK</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Alamat</td>\r\n			<td>:&nbsp;Office 8 Building, 37th floor, Sudirman CBD Lot 28, Jl. Jend. Sudirman Kav. 52-53<br />\r\n			&nbsp; (Jl. Senopati Raya 8B), Kebayoran Baru, Jakarta 12190, Indonesia.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Telepon</td>\r\n			<td>:&nbsp;(6221) 2935 6888</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Faks</td>\r\n			<td>:&nbsp;(6221) 2935 6999</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Email</td>\r\n			<td>:&nbsp;corporate.secretary@bayan.com.sg<br />\r\n			&nbsp; marketing@bayan.com.sg</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n');

-- ----------------------------
-- Table structure for `tbl_aparatur`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_aparatur`;
CREATE TABLE `tbl_aparatur` (
  `id_aparatur` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `ket` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_aparatur`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_aparatur
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_baner`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_baner`;
CREATE TABLE `tbl_baner` (
  `id_baner` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `urutan` int(11) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_baner`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_baner
-- ----------------------------
INSERT INTO `tbl_baner` VALUES ('8', '', '1', '1.jpg');
INSERT INTO `tbl_baner` VALUES ('9', '', '2', '2.jpg');
INSERT INTO `tbl_baner` VALUES ('10', '3', '3', '3.jpg');
INSERT INTO `tbl_baner` VALUES ('11', '4', '4', '4.jpg');
INSERT INTO `tbl_baner` VALUES ('12', '5', '5', '5.jpg');

-- ----------------------------
-- Table structure for `tbl_career`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_career`;
CREATE TABLE `tbl_career` (
  `id_career` int(11) NOT NULL AUTO_INCREMENT,
  `nama_career` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_career`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_career
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_corporate`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_corporate`;
CREATE TABLE `tbl_corporate` (
  `id_corporate` int(11) NOT NULL AUTO_INCREMENT,
  `nama_corporate` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_corporate`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_corporate
-- ----------------------------
INSERT INTO `tbl_corporate` VALUES ('5', 'Our Approach', '<p><strong>The Bayan Group</strong>&nbsp;has large coal reserves and resources and constitutes one of the most reputable coal suppliers in Indonesia. &nbsp;Since the purchase of the first mining concession by its controlling shareholders in 1998, the Bayan Group has shown rapid growth to the point that it now has 5 Coal Contract of Works (CCOWs) and 16 Mining Business Permits (IUPs) with concession area totalling 126,293 hectares within East and South Kalimantans.&nbsp;</p>\r\n\r\n<p>The Bayan Group&rsquo;s coal products constitute environmentally-friendly coals managed in an integrated manner, from mine plans to coal delivery to its customers. &nbsp;Bayan&rsquo;s approach to the Health, Safety, Environment and Communities Development (HSE &ndash; CD) as follows.</p>\r\n\r\n<p><strong>Compliance with relevant HSE-CD laws and regulations</strong>&nbsp;- The main objective of HSE-CD is to reduce a company&rsquo;s risk of litigation. This is achieved by making sure the company complies with relevant regulations and by helping reduce the number of accidents occurring in the workplace.&nbsp;<br />\r\nWe carried out and monitor assessments to check compliance and use benchmarks for improve the performance. &nbsp;</p>\r\n\r\n<p><strong>Establish management tools</strong>&nbsp;- Bayan Group are committed to our vision of HSE-CD. We are focused on ensuring peoples within our operation are work safely. &nbsp;We have established management tools and processes that are specific to the protection of the HSE-CD of our workers and our communities.</p>\r\n\r\n<p><strong>Collaboration with different parties</strong>&nbsp;&ndash; We are open for any collaboration with different parties such as government, affected communities, contractors and others interested parties. The complexity of the community requires a different approach in collaboration of the parties involved. Different parties, amongst which subsidiaries, have to work together on these complex undertakings. Collaboration between subsidiaries is a necessary approach.&nbsp;</p>\r\n\r\n<p><strong>Focus on the Benefits</strong>&nbsp;- We belief that embracing this corporate value offers many benefits. It improves the brand&rsquo;s image, also can help reduce operating costs, and satisfies shareholders. Focusing on the benefits ensure that everyone at our company catch the importance of this goal.</p>\r\n\r\n<p><strong>Respect local culture&nbsp;</strong>- As a mining companies with its the business of extracting valuable natural resources, the local cultural context, is important for our approach to engagement. We take an effort to build relationships and initiate development programs. We intend to meet the expectations of local communities and respect local culture.&nbsp;</p>\r\n');

-- ----------------------------
-- Table structure for `tbl_dermaga`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_dermaga`;
CREATE TABLE `tbl_dermaga` (
  `id_dermaga` int(11) NOT NULL AUTO_INCREMENT,
  `nama_dermaga` varchar(50) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `gambar` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id_dermaga`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_dermaga
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_dewan`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_dewan`;
CREATE TABLE `tbl_dewan` (
  `id_dewan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `ket` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_dewan`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_dewan
-- ----------------------------
INSERT INTO `tbl_dewan` VALUES ('23', '123', 'President Director', '<p>asdasdasd</p>\r\n', 'teenagers-clipart-4.jpg');
INSERT INTO `tbl_dewan` VALUES ('24', 'Sukragani', 'Komisaris Utama', '<p>123123123</p>\r\n', '20-208016_student-png-college-student-cartoon-png-transparent-png.png');

-- ----------------------------
-- Table structure for `tbl_direksi`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_direksi`;
CREATE TABLE `tbl_direksi` (
  `id_direksi` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `ket` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_direksi`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_direksi
-- ----------------------------
INSERT INTO `tbl_direksi` VALUES ('21', '222', '2222', '<p>222</p>\r\n', 'unnamed.jpg');

-- ----------------------------
-- Table structure for `tbl_galeri`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_galeri`;
CREATE TABLE `tbl_galeri` (
  `id_galeri` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `gambar` varchar(225) NOT NULL,
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id_galeri`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_galeri
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_investor`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_investor`;
CREATE TABLE `tbl_investor` (
  `id_investor` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `gambar` varchar(100) DEFAULT '',
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id_investor`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_investor
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_kelola`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_kelola`;
CREATE TABLE `tbl_kelola` (
  `id_kelola` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `ket` text NOT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_kelola`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_kelola
-- ----------------------------
INSERT INTO `tbl_kelola` VALUES ('27', '123', '<p>111</p>\r\n', 'group-cartoon-young-people-teenagers-260nw-664110835.png');

-- ----------------------------
-- Table structure for `tbl_keuangan`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_keuangan`;
CREATE TABLE `tbl_keuangan` (
  `id_keuangan` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `gambar` varchar(100) DEFAULT '',
  `tanggal` date NOT NULL,
  `kategori` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_keuangan`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_keuangan
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_komite`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_komite`;
CREATE TABLE `tbl_komite` (
  `id_komite` int(11) NOT NULL AUTO_INCREMENT,
  `komite` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `ket` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_komite`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_komite
-- ----------------------------
INSERT INTO `tbl_komite` VALUES ('20', '112', '111', 'President Director', '<p>11111</p>\r\n', 'mahasiswa-png-6.png');

-- ----------------------------
-- Table structure for `tbl_lokasi`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_lokasi`;
CREATE TABLE `tbl_lokasi` (
  `id_lokasi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lokasi` varchar(50) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `gambar` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id_lokasi`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_lokasi
-- ----------------------------
INSERT INTO `tbl_lokasi` VALUES ('7', 'Sumber Daya dan Cadangan', '', 'reserves_jun19_press_release-rpm_feedback.jpg');

-- ----------------------------
-- Table structure for `tbl_lowongan`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_lowongan`;
CREATE TABLE `tbl_lowongan` (
  `id_lowongan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lowongan` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_lowongan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_lowongan
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_mining`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_mining`;
CREATE TABLE `tbl_mining` (
  `id_mining` int(11) NOT NULL AUTO_INCREMENT,
  `nama_mining` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_mining`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_mining
-- ----------------------------
INSERT INTO `tbl_mining` VALUES ('5', 'Port Facilities Overview', '<p>The control of its logistics chain has been one of the key strategies of the Bayan Group and, in this regard we operate some of the leading port facilities in Indonesia. These facilities are strategically located, incorporate quality control processes and can load vessels up to Capesize. In particular, the Balikpapan Coal Terminal is one of the largest coal terminal&#39;s coal industry and note mine-dedicated so can accept coal from a number of source.</p>\r\n');

-- ----------------------------
-- Table structure for `tbl_organisasi`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_organisasi`;
CREATE TABLE `tbl_organisasi` (
  `id_org` int(11) NOT NULL AUTO_INCREMENT,
  `nama_org` varchar(20) NOT NULL,
  `ket` text NOT NULL,
  PRIMARY KEY (`id_org`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_organisasi
-- ----------------------------
INSERT INTO `tbl_organisasi` VALUES ('8', 'SEKSI PEMBERDAYAAN', '<p><strong>-</strong></p>\r\n');
INSERT INTO `tbl_organisasi` VALUES ('13', 'SEKSI LINJAMSOS', '');
INSERT INTO `tbl_organisasi` VALUES ('14', 'SEKSI RESHOS', '');
INSERT INTO `tbl_organisasi` VALUES ('16', 'Bayan Overview', '<p>The business of the Bayan Group is strongly built on the strength of our experience in Indonesia. Our President Director and Founder of Bayan Group - Dato&rsquo; Dr. Low Tuck Kwong&rsquo;s business interests commenced in Indonesia in 1973 when he formed PT. Jaya Sumpiles Indonesia (JSI) as an earthworks, civil works and marine structure contractor. JSI quickly became pioneer in complex pile foundation works, and leading contractor in Indonesia in the above fields and remained so during the 1980&rsquo;s and 1990&rsquo;s. In 1988, JSI ventured into contract coal mining and was a leading mining contractor until 1998 when Dato&rsquo; Dr. Low acquired PT. Gunung Bayan Pratamacoal (GBP) and PT. Dermaga Perkasapratama (DPP). At the time GBP had not commenced mining and the Balikpapan Coal Terminal (under DPP) has a rated capacity of 2.5 million tonnes per annum.</p>\r\n\r\n<p>Under the leadership of Dato&rsquo; Dr. Low, the Bayan Group quickly transformed into vertically integrated successful and reputed coal mining company. The Bayan Group was formed through number of strategic acquisitions in the coal sector and established with a proven track record in developing greenfield coal mines.</p>\r\n\r\n<p>In particular, in recent years the Tabang / Pakar project has expanded from being a small-scale mining operation producing only 1.9 million tonnes in 2014 to producing approximately 22.7 million tonnes in 2018. This has rapidly increased Bayan&rsquo;s position in the market now placing it within the top 5 Indonesian coal producers. Further growth is planned in coming years with a target to increase the Tabang / Pakar Project to production of approximately 50 million tonnes per annum.</p>\r\n\r\n<p>The Bayan Group is an innovator in the Indonesian coal mining industry and it is continuously looking at new methodologies and technologies to cement its position as one of the lowest cost producers in Indonesia. For example the Bayan Group was the first coal mining company in Indonesia to employ through-seam blasting at its Wahana mine, utilise the dozer-push mining methodologies at our Tabang mine and is currently utilising some of the largest coal haulage trucks in Indonesia with payloads of 200 to 220 tonnes. We expect this trend to continue as we evaluate other opportunities.</p>\r\n\r\n<p>The Bayan Group has some of the leading coal infrastructure in the country through its ownership of the Balikpapan Coal Terminal, the Perkasa and Wahana Jetties and the two Floating Transfer Barges (KFT&rsquo;s). These facilities including the ability to unload barges, stockpile coal and load to vessels at speeds ranging from 3,000 to 8,000 tonnes per hour. Certain of these facilities are also able to blend coal from multiples stockpiles and qualities. We continue to invest to expand these facilities where required providing our customers flexibility in the vessels they use as well as redundancy in terms of our capacity.</p>\r\n');

-- ----------------------------
-- Table structure for `tbl_penghargaan`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_penghargaan`;
CREATE TABLE `tbl_penghargaan` (
  `id_penghargaan` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `gambar` varchar(225) NOT NULL,
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id_penghargaan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_penghargaan
-- ----------------------------
INSERT INTO `tbl_penghargaan` VALUES ('4', 'Penghargaan Program P2-HIV & AIDS PT WBM -', 'wbm_p2_hiv-aids_kemenaker.jpg', '2020-08-22');

-- ----------------------------
-- Table structure for `tbl_port`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_port`;
CREATE TABLE `tbl_port` (
  `id_port` int(11) NOT NULL AUTO_INCREMENT,
  `nama_port` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_port`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_port
-- ----------------------------
INSERT INTO `tbl_port` VALUES ('5', 'Port Facilities Overview', '<p>The control of its logistics chain has been one of the key strategies of the Bayan Group and, in this regard we operate some of the leading port facilities in Indonesia. These facilities are strategically located, incorporate quality control processes and can load vessels up to Capesize. In particular, the Balikpapan Coal Terminal is one of the largest coal terminal&#39;s coal industry and note mine-dedicated so can accept coal from a number of source.</p>\r\n');

-- ----------------------------
-- Table structure for `tbl_publik`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_publik`;
CREATE TABLE `tbl_publik` (
  `id_layanan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_layanan` varchar(200) NOT NULL,
  `ket` text NOT NULL,
  `lampiran` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_layanan`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_publik
-- ----------------------------
INSERT INTO `tbl_publik` VALUES ('2', 'Bayan Coal Overview', 'Bayan Coal Overview\r\nBayan Resources is engaged in open cut mining of four main projects located in East and South Kalimantan, Indonesia. As an integrated coal producer, Bayan produces coal ranging from high calorific value bituminous coal to sub-bituminous low-sulphur, low-ash coal.\r\n \r\nThe Tabang Concession, the Bayan Group’s key producing asset today contributing approximately 80% of Bayan’s coal production, is one of Indonesia’s lowest cost coal mines, and is uniquely positioned to expand capacity rapidly with low capital expenditure.\r\n\r\nWe own and operate all of the key infrastructure facilities that interlink our logistics chain and ensure that we are able to deliver our coal to our customers to specification.\r\n\r\nWe benefit from being strategically located close to our key customers markets in Asia and with our considerable reserve base, long concession lives and reliable supply this has made us a preferred supplier to many of the regions leading power stations.', '2');
INSERT INTO `tbl_publik` VALUES ('16', 'Marketing Strategy', 'Marketing Strategy\r\nWe handle our sales and marketing in-house, with assistance from marketing agents, and employ a sales strategy of maintaining a mix of end-customers, such as utility and industrial companies, and commodity trading companies.\r\n\r\nWe have a broad spread of countries to which we supply to spread our risk which include Indonesia, Malaysia, India, China, Vietnam, the Philippines, Japan, Korea, Spain and Taiwan.\r\n\r\nWe maintain a significant order books of long-term contracts up to a maximum of 25 years and covering more than 300 million tonnes going forward.  These contracts are linked to Newcastle, API4, API5, Indonesian Minimum Price (HBA) or are annually negotiated.\r\n\r\nOur Tabang / Pakar coals are sub-bituminous coal (4,000 – 4,400 Kcal/kg GAR), low ash (4 – 8%) and low sulphur (0.12 – 0.15%) and well-suited for today’s modern supercritical power stations.', '1');

-- ----------------------------
-- Table structure for `tbl_publikasi`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_publikasi`;
CREATE TABLE `tbl_publikasi` (
  `id_publikasi` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) NOT NULL,
  `ket` text NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id_publikasi`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_publikasi
-- ----------------------------
INSERT INTO `tbl_publikasi` VALUES ('1', '1', '1', '02_28_25pm01_Jenis_Pelayanan_Di_Kecamatan.jpg', '2020-08-03');

-- ----------------------------
-- Table structure for `tbl_rilis`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_rilis`;
CREATE TABLE `tbl_rilis` (
  `id_rilis` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `gambar` varchar(100) DEFAULT '',
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id_rilis`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_rilis
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_rups`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_rups`;
CREATE TABLE `tbl_rups` (
  `id_rups` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `gambar` varchar(100) DEFAULT '',
  `tanggal` date NOT NULL,
  `kategori` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_rups`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_rups
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_saham`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_saham`;
CREATE TABLE `tbl_saham` (
  `id_saham` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `gambar` varchar(100) DEFAULT '',
  `tanggal` date NOT NULL,
  `kategori` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_saham`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_saham
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_struktur`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_struktur`;
CREATE TABLE `tbl_struktur` (
  `id_struktur` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `ket` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_struktur`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_struktur
-- ----------------------------
INSERT INTO `tbl_struktur` VALUES ('18', 'Struktur Korporasi', '<p>Struktur korporasi Bayan Group saat ini terdiri atas 30 perusahaan yang mencakup perusahaan tambang, investasi, pengapalan, penanganan batubara dan jasa. &nbsp;Bayan merupakan pemilik mayoritas semua perusahaan ini, sehingga memegang kendali atas aset pertambangan dan infrastruktur penting.&nbsp;</p>\r\n', 'ar_bayan_hal_80-81_ojk.jpg');
INSERT INTO `tbl_struktur` VALUES ('19', 'Struktur Organisasi', '<p>Bayan Group saat ini memiliki karyawan langsung lebih dari 2.000 orang yang terutama beroperasi di bidang manajemen, geologi, pemeliharaan, perencanaan tambang, purchasing, keuangan, akuntansi, pajak dan kegiatan pendukung lainnya. Bayan Group saat ini menggunakan jasa kontraktor untuk melakukan pengangkatan overburden, sewa peralatan dan jasa pengangkutan batubara. Total tenaga kerja kontraktor adalah lebih dari 6.000 personel.&nbsp;</p>\r\n', 'orgchart_2019.jpg');

-- ----------------------------
-- Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `level` varchar(12) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO `tbl_user` VALUES ('2', 'Administrator', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3');
