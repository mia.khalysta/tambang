<div class="page-wrapper">

	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-12 d-flex no-block align-items-center">
				<h4 class="page-title"><?php echo $title ?></h4>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<?php echo form_open_multipart('baner/update') ?>

				<div class="form-group">

					<input type="hidden" value="<?php echo $data_baner->id_baner ?>" name="id_baner">
				</div>

				<div class="form-group">
					<input type="hidden" name="TxtJudul" value="<?php echo $data_baner->judul ?>" class="form-control" placeholder="Masukkan Nama baner" readonly>
				</div>

				<div class="form-group">
					<input type="hidden" name="TxtUrutan" value="<?php echo $data_baner->urutan ?>" class="form-control" placeholder="Masukkan Nama baner" readonly>
				</div>

				<div class="form-group">
					<label for="text">Gambar</label><br />
					<input type="file" name="berkas" class="form-control" accept="image/*">
					<input type="hidden" name="berkas_lama" value="<?php echo $data_baner->gambar ?>" class="form-control">

				</div>

				<button type="submit" class="btn btn-md btn-success">Update</button>
				<button type="reset" class="btn btn-md btn-warning">reset</button>
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>