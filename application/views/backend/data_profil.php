<div class="page-wrapper">

    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title"><?php echo $title ?></h4>
            </div>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">
                        <?php echo $this->session->flashdata('notif') ?>

                        <form action="" class="form-horizontal">
                            <div class="form-group">
                                <input type="hidden" name="id" value="<?php echo $hasil->id_user ?>">
                            </div>
                            <div class="row" style="padding-bottom: 13px;">
                                <div class="col-md-2">
                                    <label for="text" style="padding-top: 5px;">Nama Lengkap</label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" disabled name="nama" value="<?php echo $hasil->nama ?>" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 13px;">
                                <div class="col-md-2">
                                    <label for="text" style="padding-top: 5px;">Username</label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" disabled name="username" value="<?php echo $hasil->username ?>" class="form-control">
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 13px;">
                                <div class="col-md-2">
                                    <label for="text" style="padding-top: 5px;">Pasword</label>
                                </div>
                                <div class="col-md-5">
                                    <input type="password" disabled name="pass" value="<?php echo $hasil->password ?>" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 col-md-offset-2">
                                    <!--<button style="float: right; width: 70px;" type="submit" class="btn btn-md btn-success">Edit</button>-->
                                    <a style="float: right;" href="<?php echo base_url() ?>login/edit/<?php echo $hasil->id_user ?>" class="btn btn-md btn-success"> <i class="fa fa-edit m-r-5 m-l-5"></i> Edit &nbsp;</a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>