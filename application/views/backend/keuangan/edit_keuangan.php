<div class="page-wrapper">

	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-12 d-flex no-block align-items-center">
				<h4 class="page-title"><?php echo $title ?></h4>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<?php echo form_open_multipart('keuangan/update') ?>
				<div class="form-group">
					<input type="hidden" value="<?php echo $data_keuangan->id_keuangan ?>" name="id_keuangan">
				</div>
				<div class="form-group">
				<label for="text">Judul</label>
					<input type="text" name="TxtNama" value="<?php echo $data_keuangan->judul ?>" class="form-control" placeholder="Masukkan Nama">
				</div>
				<div class="form-group">
					<label for="text">Dokumen</label><br />
					<input type="file" name="berkas" class="form-control">
					<input type="hidden" name="berkas_lama" value="<?php echo $data_keuangan->gambar ?>" class="form-control">
				</div>
				<button type="submit" class="btn btn-md btn-success">Update</button>
				<button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>