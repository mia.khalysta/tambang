<div class="page-wrapper">

    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title"><?php echo $title ?></h4>
            </div>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <?php echo $this->session->flashdata('notif') ?>

                <?php
                if (($this->session->userdata("level")) == 'superadmin') {
                    ?>
                    <?php echo form_open_multipart('login/update_p') ?>
                <?php
                } else {
                    ?>
                    <?php echo form_open_multipart('login/update') ?>
                <?php
                }
                ?>

                <div class="form-group">

                    <input type="hidden" name="id" value="<?php echo $data_p->id_user ?>">
                </div>
                <div class="form-group">
                    <label for="text">Nama Lengkap</label>
                    <input type="text" name="nama" value="<?php echo $data_p->nama ?>" class="form-control" placeholder="Masukkan Nama tipe" autofocus>
                </div>

                <div class="form-group">
                    <label for="text">Username</label>
                    <input type="text" name="username" value="<?php echo $data_p->username ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label for="text">Password</label>
                    <input type="password" name="pass" class="form-control">
                </div>

                <button type="submit" class="btn btn-md btn-success">Update</button>
                <button type="reset" class="btn btn-md btn-warning">reset</button>

                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>