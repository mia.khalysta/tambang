<div class="page-wrapper">

    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title"><?php echo $title ?></h4>
            </div>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <?php echo $this->session->flashdata('notif') ?>
                <?php echo form_open_multipart('login/simpan') ?>
                <div class="form-group">
                    <label for="text">Nama Lengkap</label>
                    <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama" autofocus>
                </div>

                <div class="form-group">
                    <label for="text">Username</label>
                    <input type="text" name="username" class="form-control">
                </div>
                <div class="form-group">
                    <label for="text">Password</label>
                    <input type="password" name="pass" class="form-control">
                </div>

                <button type="submit" class="btn btn-md btn-success">Simpan</button>
                <button type="reset" class="btn btn-md btn-warning">reset</button>

                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>