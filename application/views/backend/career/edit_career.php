<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="page-wrapper">

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12 card">
			<div class="card-body">
                <?php echo form_open_multipart('career/update') ?>
                <div class="form-group">
                    <label for="text">Nama Career</label>
                    <input type="text" name="Txtcareer" class="form-control" value="<?= $data_jb->nama_career ?>" required autofocus>
                    <input type="hidden" name="TxtIDcareer" class="form-control" value="<?= $data_jb->id_career ?>">
                </div>
				<div class="form-group">
                    <label for="text">Keterangan</label>
                    <textarea name="Txtket" class="ckeditor" id="ckedtor" placeholder="Masukkan Keterangan"><?= $data_jb->keterangan ?></textarea>
                </div>
			</div>	
			<div class="border-top card-body">	
                <button type="submit" class="btn btn-md btn-success">Simpan</button>
                <button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
                <?php echo form_close() ?>
			</div>	
            </div>
        </div>
    </div>
</div>