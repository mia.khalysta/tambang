<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="page-wrapper">

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12 card">
			<div class="card-body">
			<?php echo $this->session->flashdata('notif') ?>
                <?php echo form_open_multipart('career/simpan') ?>
                <div class="form-group">
                    <label for="text">Nama Career</label>
                    <input type="text" name="Txtcareer" class="form-control" placeholder="Masukkan Nama career" required autofocus>
                </div>
				<div class="form-group">
                    <label for="text">Keterangan</label>
                    <textarea class="ckeditor" id="ckedtor" name="Txtket" class="form-control" placeholder="Masukkan Keterangan" ></textarea>
                </div>
			</div>	
			<div class="border-top card-body">	
                <button type="submit" class="btn btn-md btn-success">Simpan</button>
                <button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
                <?php echo form_close() ?>
			</div>	
            </div>
        </div>
    </div>
</div>