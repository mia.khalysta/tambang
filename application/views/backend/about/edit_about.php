<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="page-wrapper">

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12 card">
			<div class="card-body">
                <?php echo form_open_multipart('about/update') ?>
                <div class="form-group">
                    <label for="text">Judul</label>
                    <input type="text" name="Txtabout" class="form-control" value="<?= $data_jb->nama_about ?>" required autofocus>
                    <input type="hidden" name="TxtIDabout" class="form-control" value="<?= $data_jb->id_about ?>">
                </div>
				<div class="form-group">
                    <label for="text">Keterangan</label>
                    <textarea name="Txtket" class="ckeditor" id="ckedtor" placeholder="Masukkan Keterangan"><?= $data_jb->keterangan ?></textarea>
                </div>
			</div>	
			<div class="border-top card-body">	
                <button type="submit" class="btn btn-md btn-success">Simpan</button>
                <button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
                <?php echo form_close() ?>
			</div>	
            </div>
        </div>
    </div>
</div>