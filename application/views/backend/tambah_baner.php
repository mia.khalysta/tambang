<div class="page-wrapper">

	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-12 d-flex no-block align-items-center">
				<h4 class="page-title"><?php echo $title ?></h4>
			</div>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-md-12">
				<?php echo form_open_multipart('baner/simpan') ?>
				<div class="form-group">
					<label for="text">Judul</label>
					<input type="text" name="TxtJudul" class="form-control" placeholder="Masukkan Nama baner" required autofocus>
				</div>

				<div class="form-group">
					<label for="text">Urutan</label>
					<input type="number" name="TxtUrutan" class="form-control" required>
				</div>

				<div class="form-group">
					<label for="text">Gambar</label>
					<input type="file" name="berkas" class="form-control" required accept="image/*">
				</div>

				<button type="submit" class="btn btn-md btn-success">Simpan</button>
				<button type="reset" class="btn btn-md btn-warning">reset</button>
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>