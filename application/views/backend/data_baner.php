<div class="page-wrapper">

	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-12 d-flex no-block align-items-center">
				<h4 class="page-title"><?php echo $title ?></h4>
			</div>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-12">

				<div class="card">
					<div class="card-body">
						<?php echo $this->session->flashdata('notif') ?>
						<hr>
						<div class="table-responsive">
							<table id="zero_config" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>No.</th>
										<th>Urutan</th>
										<th>Slider</th>
										<th>Options</th>
									</tr>
								</thead>
								<tbody>

									<?php
									$no = 1;
									foreach ($data_baner as $hasil) {
										?>

										<tr>
											<td style="width: 8%;"><?php echo $no++ ?></td>
											<td style="width: 10%;"><?php echo $hasil->urutan ?></td>
											<td><img src="<?php echo base_url() . "files/slide/" . $hasil->gambar ?>" width="80"></td>
											<td style="width: 20%;">
												<a href="<?php echo base_url() ?>baner/edit/<?php echo $hasil->id_baner ?>" class="btn btn-sm btn-success">Edit</a>
											</td>
										</tr>

									<?php } ?>

								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>