<div class="page-wrapper">

	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-12 d-flex no-block align-items-center">
				<h4 class="page-title"><?php echo $title ?></h4>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<?php echo form_open_multipart('aparatur/update') ?>

				<div class="form-group">

					<input type="hidden" value="<?php echo $data_aparatur->id_aparatur ?>" name="id_aparatur">
				</div>

				<div class="form-group">
				<label for="text">Nama</label>
					<input type="text" name="TxtNama" value="<?php echo $data_aparatur->nama ?>" class="form-control" placeholder="Masukkan Nama aparatur">
				</div>
				<div class="form-group">
				<label for="text">Jabatan</label>
					<input type="text" name="TxtJabatan" value="<?php echo $data_aparatur->jabatan ?>" class="form-control" placeholder="Masukkan Nama aparatur">
				</div>
				<div class="form-group">
					<label for="text">Gambar</label><br />
					<input type="file" name="berkas" class="form-control" accept="image/*">
					<input type="hidden" name="berkas_lama" value="<?php echo $data_aparatur->gambar ?>" class="form-control">
				</div>
				<div class="form-group">
                    <label for="text">Keterangan</label>
                    <textarea name="TxtKet" class="ckeditor" id="ckedtor" placeholder="Masukkan Keterangan"><?= $data_aparatur->ket ?></textarea>
                </div>

				

				<button type="submit" class="btn btn-md btn-success">Update</button>
				<button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>