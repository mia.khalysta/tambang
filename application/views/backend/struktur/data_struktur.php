<div class="page-wrapper">

	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-12 d-flex no-block align-items-center">
				<h4 class="page-title"><?php echo $title ?></h4>
			</div>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-12">

				<div class="card">
					<div class="card-body">
						<?php echo $this->session->flashdata('notif') ?>
						<a href="<?php echo base_url() ?>tambahstruktur" class="btn btn-md btn-success"><span class="glyphicon glyphicon-plus"></span> Tambah</a>
						<hr>
						<div class="table-responsive">
							<table id="zero_config" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>No.</th>
										<th>Judul</th>
										<th>Keterangan</th>
										<th>Gambar</th>
										<th>Options</th>
									</tr>
								</thead>
								<tbody>

									<?php
									$no = 1;
									foreach ($data_struktur as $hasil) {
										?>

										<tr>
											<td style="width: 8%;"><?php echo $no++ ?></td>
											<td style="width: 10%;"><?php echo $hasil->judul ?></td>
											<td style="width: 10%;"><?php echo $hasil->ket ?></td>
											<td><img src="<?php echo base_url() . "files/struktur/" . $hasil->gambar ?>" width="100%"></td>
											<td style="width: 20%;">
												<a href="<?php echo base_url() ?>struktur/edit/<?php echo $hasil->id_struktur ?>" class="btn btn-sm btn-success">Edit</a>
												<a href="<?php echo base_url() ?>struktur/hapus/<?php echo $hasil->id_struktur ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin menghapus <?php echo $hasil->judul ?> ?')">Hapus</a>
											</td>
										</tr>

									<?php } ?>

								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>