<div class="page-wrapper">

	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-12 d-flex no-block align-items-center">
				<h4 class="page-title"><?php echo $title ?></h4>
			</div>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-md-12">
				<?php echo form_open_multipart('struktur/simpan') ?>
				<div class="form-group">
					<label for="text">Judul</label>
					<input type="text" name="TxtJudul" class="form-control" placeholder="Masukkan Nama Struktur" required autofocus>
				</div>

				<div class="form-group">
					<label for="text">Gambar</label>
					<input type="file" name="berkas" class="form-control" required accept="image/*">
				</div>
				
				<div class="form-group">
                    <label for="text">Keterangan</label>
                    <textarea class="ckeditor" id="ckedtor" name="TxtKet" class="form-control" placeholder="Masukkan Keterangan" ></textarea>
                </div>
				
				<button type="submit" class="btn btn-md btn-success">Simpan</button>
				<button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>