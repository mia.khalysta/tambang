<div class="page-wrapper">

	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-12 d-flex no-block align-items-center">
				<h4 class="page-title"><?php echo $title ?></h4>
			</div>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-12">

				<div class="card">
					<div class="card-body">
						<?php echo $this->session->flashdata('notif') ?>
						<a href="<?php echo base_url() ?>tambahrups" class="btn btn-md btn-success"><span class="glyphicon glyphicon-plus"></span> Tambah</a>
						<hr>
						<div class="table-responsive">
							<table id="zero_config" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>No.</th>
										<th>Kategori</th>
										<th>Judul</th>
										<th>Dokumen</th>
										<th>Tanggal</th>
										<th>Options</th>
									</tr>
								</thead>
								<tbody>

									<?php
									$no = 1;
									foreach ($data_rups as $hasil) {
										?>

										<tr>
											<td style="width: 8%;"><?php echo $no++ ?></td>
											<td style="width: 10%;"><?php echo $hasil->kategori ?></td>
											<td style="width: 10%;"><?php echo $hasil->judul ?></td>
											<td style="width: 20%;"><a href="<?php echo base_url() . "files/rups/" . $hasil->gambar ?>" target="_blank"><?php echo $hasil->gambar ?></a></td>
											<td style="width: 10%;"><?php echo date('d M Y',strtotime($hasil->tanggal))?></td>
											<td style="width: 20%;">
												<a href="<?php echo base_url() ?>rups/edit/<?php echo $hasil->id_rups ?>" class="btn btn-sm btn-success">Edit</a>
												<a href="<?php echo base_url() ?>rups/hapus/<?php echo $hasil->id_rups ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin menghapus <?php echo $hasil->judul ?> ?')">Hapus</a>
											</td>
										</tr>

									<?php } ?>

								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>