<div class="page-wrapper">

	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-12 d-flex no-block align-items-center">
				<h4 class="page-title"><?php echo $title ?></h4>
			</div>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-12">

				<div class="card">
					<div class="card-body">
						<?php echo $this->session->flashdata('notif') ?>
						<a href="<?php echo base_url() ?>tambahpublikasi" class="btn btn-md btn-success">Tambah</a>
						<hr>
						<div class="table-responsive">
							<table id="zero_config" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>No.</th>
										<th>Judul</th>
										<th>Tanggal</th>
										<th>Gambar</th>
										<th>Keterangan</th>
										<th>Options</th>
									</tr>
								</thead>
								<tbody>

									<?php
									$no = 1;
									foreach ($data_publikasi as $hasil) {
										?>

										<tr>
											<td style="width: 8%;"><?php echo $no++ ?></td>
											<td style="width: 10%;"><?php echo $hasil->judul ?></td>
											<td style="width: 10%;"><?php echo date("d/M/Y",strtotime($hasil->tanggal)) ?></td>
											<td><img src="<?php echo base_url() . "files/publikasi/" . $hasil->gambar ?>" width="100%"></td>
											<td style="width: 10%;"><?php echo $hasil->ket ?></td>
											<td style="width: 20%;">
												<a href="<?php echo base_url() ?>publikasi/edit/<?php echo $hasil->id_publikasi ?>" class="btn btn-sm btn-success">Edit</a>
												<a href="<?php echo base_url() ?>publikasi/hapus/<?php echo $hasil->id_publikasi ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin menghapus <?php echo $hasil->judul ?> ?')">Hapus</a>
											</td>
										</tr>

									<?php } ?>

								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>