<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>	<div class="page-wrapper">

    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title"><?php echo $title ?></h4>
            </div>
        </div>
    </div>

		<div class="container-fluid">
                <div class="row">
                    <!-- Primary table start -->
                    <div class="col-12 card">
                        <div class="">
                            <div class="card-body">
							<?php echo $this->session->flashdata('notif') ?>
							<a href="<?php echo base_url() ?>tambahcorporate" class="btn btn-md btn-success"><span class="glyphicon glyphicon-plus"></span> Tambah</a>
							<hr>
                                <div class="table-responsive">
									<table id="zero_config" class="table table-striped table-bordered">
                                        <thead class="text-capitalize">
                                            <tr>
												<th>No.</th>
												<th>Judul</th>
												<th>Keterangan</th>
												<th>Options</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($data_corporate as $hasil) {
												?>
												<tr>
													<td style="width: 8%;"><?php echo $no++ ?></td>
													<td><?php echo $hasil->nama_corporate ?></td>
													<td><?php echo $hasil->keterangan ?></td>
													<td style="width: 20%;">
														<a href="<?php echo base_url() ?>corporate/edit/<?php echo $hasil->id_corporate ?>" class="btn btn-sm btn-success">Edit</a>
														<a href="<?php echo base_url() ?>corporate/hapus/<?php echo $hasil->id_corporate ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin menghapus <?php echo $hasil->nama_corporate ?> ?')">Hapus</a>
													</td>
												</tr>
											<?php } ?>
										</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Primary table end -->
                </div>
        </div>
    </div>
</div>