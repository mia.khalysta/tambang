<div class="page-wrapper">

	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-12 d-flex no-block align-items-center">
				<h4 class="page-title"><?php echo $title ?></h4>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<?php echo form_open_multipart('publikasi/update') ?>
				<div class="form-group">
					<input type="hidden" value="<?php echo $data_publikasi->id_publikasi ?>" name="id_publikasi">
				</div>
				<div class="form-group">
				<label for="text">Judul</label>
					<input type="text" name="TxtJudul" value="<?php echo $data_publikasi->judul ?>" class="form-control" placeholder="Masukkan Nama Publikasi">
				</div>
				<div class="form-group">
                    <label for="text">Tanggal Kegiatan</label>
					<input type="date" name="TxtTanggal" value="<?php echo $data_publikasi->tanggal ?>" class="form-control">
                </div>
				<div class="form-group">
					<label for="text">Gambar</label><br />
					<input type="file" name="berkas" class="form-control" accept="image/*">
					<input type="hidden" name="berkas_lama" value="<?php echo $data_publikasi->gambar ?>" class="form-control">
				</div>				
				<div class="form-group">
                    <label for="text">Keterangan</label>
                    <textarea name="TxtKet" class="ckeditor" id="ckedtor" placeholder="Masukkan Keterangan"><?= $data_publikasi->ket ?></textarea>
                </div>
				<button type="submit" class="btn btn-md btn-success">Update</button>
				<button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>