<div class="page-wrapper">

	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-12 d-flex no-block align-items-center">
				<h4 class="page-title"><?php echo $title ?></h4>
			</div>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-12">

				<div class="card">
					<div class="card-body">
						<?php echo $this->session->flashdata('notif') ?>
						<a href="<?php echo base_url() ?>tambahkomite" class="btn btn-md btn-success"><span class="glyphicon glyphicon-plus"></span> Tambah</a>
						<hr>
						<div class="table-responsive">
							<table id="zero_config" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>No.</th>
										<th>Judul</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Keterangan</th>
										<th>Gambar</th>
										<th>Options</th>
									</tr>
								</thead>
								<tbody>

									<?php
									$no = 1;
									foreach ($data_komite as $hasil) {
										?>

										<tr>
											<td style="width: 8%;"><?php echo $no++ ?></td>
											<td style="width: 10%;"><?php echo $hasil->komite ?></td>
											<td style="width: 10%;"><?php echo $hasil->nama ?></td>
											<td style="width: 10%;"><?php echo $hasil->jabatan ?></td>
											<td style="width: 60%;"><?php echo $hasil->ket ?></td>
											<td style="width: 20%;"><img src="<?php echo base_url() . "files/komite/" . $hasil->gambar ?>" width="100%"></td>
											<td style="width: 20%;">
												<a href="<?php echo base_url() ?>komite/edit/<?php echo $hasil->id_komite ?>" class="btn btn-sm btn-success">Edit</a>
												<a href="<?php echo base_url() ?>komite/hapus/<?php echo $hasil->id_komite ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin menghapus <?php echo $hasil->nama ?> ?')">Hapus</a>
											</td>
										</tr>

									<?php } ?>

								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>