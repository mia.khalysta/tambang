    <aside class="left-sidebar" data-sidebarbg="skin5">
        <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>admin" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>						
						<li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-basecamp"></i><span class="hide-menu">Profil </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
								<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>datavisimisi" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Visi Misi</span></a></li>
								<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>datakelurahan" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Kelurahan/Desa</span></a></li>							
                            </ul>
                        </li>					
						<li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-key"></i><span class="hide-menu">Pelayanan</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
								<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>datapublikasi" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">Info Publik</span></a></li>								
								<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>datalayananpublik" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">SOP Layanan Publik</span></a></li>
								<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>dataregulasi" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">Dokumen</span></a></li>
								<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>datagaleri" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">Galeri</span></a></li>
                            </ul>
                        </li>
						<li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-key"></i><span class="hide-menu">administrasi</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
								<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>datakategori" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">Kategori Menu</span></a></li>
								<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>dataorganisasi" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">Orgnisasi</span></a></li>

								<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>login/dataprofil/<?php echo $this->session->userdata("idU"); ?>" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">Profil Admin</span></a></li>
								<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('login/logout'); ?>" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">Logout</span></a></li>
                            </ul>
                        </li>
						<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>dataabout" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">About</span></a></li>
						<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>datamining" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">Coal Mining</span></a></li>
						<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>dataport" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">Port Facilities</span></a></li>
						<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>datacorporate" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">Corporate Social Responsibility</span></a></li>
						<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>datacareer" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu">Career</span></a></li>
						<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>datastruktur" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Struktur Organisasi</span></a></li>
						<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url() ?>dataaparatur" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Sambutan Aparatur</span></a></li>
                    </ul>
                </nav>
        </div>
    </aside>