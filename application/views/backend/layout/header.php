<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>vendor/images/favicon.png">
    <title>Operator </title>
  <!-- Bootstrap core CSS-->

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url() ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url() ?>css/sb-admin.css" rel="stylesheet">

	<link href="<?php echo base_url() ?>vendor/libs/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>vendor/extra-libs/calendar/calendar.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>vendor/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>dist/css/style.min.css" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/libs/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/libs/jquery-minicolors/jquery.minicolors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<style>
	.table thead th {
		vertical-align: middle;
		text-align:center;
		font-weight: bolder;
	}

	.table td, .table th {
		vertical-align: middle;
	}
	</style>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">