<!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small style="font-size:14px;">
				&copy; 
				<?php
					$fromYear = 2020; 
					$thisYear = (int)date('Y'); 
					echo $fromYear . (($fromYear != $thisYear) ? ' - ' . $thisYear : '');
				?>
				Karya Informatika
				</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="<?php echo base_url('login/logout'); ?>">Logout</a>
          </div>
        </div>
      </div>
    </div>

            <script src="<?php echo base_url() ?>vendor/libs/jquery/dist/jquery.min.js"></script>
            <!-- Bootstrap tether Core JavaScript -->
            <script src="<?php echo base_url() ?>vendor/libs/bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- this page js -->
            <script src="<?php echo base_url() ?>vendor/libs/moment/min/moment.min.js"></script>
            <!-- table js -->
            <script src="<?php echo base_url() ?>vendor/extra-libs/DataTables/datatables.min.js"></script>
			<!-- ckeditor js -->
            <script type="text/javascript" src="<?php echo base_url() ?>vendor/libs/ckeditor/ckeditor.js"></script>

			<!-- Bootstrap core JavaScript-->
			<script src="<?php echo base_url() ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
			<!-- Core plugin JavaScript-->
			<script src="<?php echo base_url() ?>vendor/jquery-easing/jquery.easing.min.js"></script>
			<!-- Page level plugin JavaScript-->
			<script src="<?php echo base_url() ?>vendor/chart.js/Chart.min.js"></script>
			<!-- Custom scripts for all pages-->
			<script src="<?php echo base_url() ?>js/sb-admin.min.js"></script>
			<!-- Custom scripts for this page-->
			<script src="<?php echo base_url() ?>js/sb-admin-charts.min.js"></script>
            <script>
                /****************************************
                 *       Basic Table                   *
                 ****************************************/
                $('#zero_config').DataTable();
            </script>

            <script>
                $('#id_merek').onload(function() {
                    var id_merek = $(this).val();
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>produk/dataTipe",
                        dataType: "JSON",
                        data: {
                            id_merek: id_merek
                        },
                        cache: false,
                        success: function(data) {
                            $.each(data, function(id_merek, nm_tipe, harga) {
                                $('[name=nm_tipe]').val(data.nm_tipe);
                                $('[name=harga]').val(data.harga);
                            });
                        }
                    });
                    return false;
                });
            </script>

    
		<!-- Bootstrap tether Core JavaScript -->
		<!-- slimscrollbar scrollbar JavaScript -->
		<script src="<?php echo base_url() ?>vendor/libs/select2/dist/js/select2.full.min.js"></script>
		<script src="<?php echo base_url() ?>vendor/libs/select2/dist/js/select2.min.js"></script>
		<script src="<?php echo base_url() ?>vendor/libs/jquery-minicolors/jquery.minicolors.min.js"></script>
		<script src="<?php echo base_url() ?>vendor/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
		<script>
			//***********************************//
			// For select 2
			//***********************************//
			$(".select2").select2();

			/*colorpicker*/
			$('.demo').each(function() {
			//
			// Dear reader, it's actually very easy to initialize MiniColors. For example:
			//
			//  $(selector).minicolors();
			//
			// The way I've done it below is just for the demo, so don't get confused
			// by it. Also, data- attributes aren't supported at this time...they're
			// only used for this demo.
			//
			$(this).minicolors({
					control: $(this).attr('data-control') || 'hue',
					position: $(this).attr('data-position') || 'bottom left',

					change: function(value, opacity) {
						if (!value) return;
						if (opacity) value += ', ' + opacity;
						if (typeof console === 'object') {
							console.log(value);
						}
					},
					theme: 'bootstrap'
				});

			});
			/*datwpicker*/
			jQuery('.mydatepicker').datepicker({
			format: 'dd/mm/yyyy'
			});
			jQuery('#datepicker-autoclose').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true,
				todayHighlight: true
			});
			var quill = new Quill('#editor', {
				theme: 'snow'
			});

		</script>
	
		</div>
    </body>
</html>