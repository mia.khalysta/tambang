  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <h4 style="color:#fff;">Halaman Administrator</h4>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
	<!-- Navigation-->
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> 
		<a class="nav-link" href="<?php echo base_url() ?>admin" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i> <span class="hide-menu">Dashboard</span></a></li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> 
		<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#manajemen" data-parent="#exampleAccordion"><i class="mdi mdi-account-key"></i> <span class="hide-menu">Tentang</span></a>
		  <ul class="sidenav-second-level collapse" id="manajemen">
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>dataabout" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Tentang Perusahaan</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datastruktur" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Struktur Organisasi</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datakomisaris" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Profil Komisaris</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datadireksi" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Profil Direksi</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datakomite" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Profil Komite</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datakelola" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Tata Kelola</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datapenghargaan" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Penghargaan</span></a></li>
		  </ul>
        </li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> 
		<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#mining" data-parent="#exampleAccordion"><i class="mdi mdi-account-key"></i> <span class="hide-menu">Penambangan</span></a>
		  <ul class="sidenav-second-level collapse" id="mining">
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datamining" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Info Penambangan</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datalokasi" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Lokasi/Sumber Daya</span></a></li>
		  </ul>
        </li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datadermaga" aria-expanded="false"><i class="mdi mdi-relative-scale"></i> <span class="hide-menu">Fasilitas Pelabuhan</span></a></li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datacorporate" aria-expanded="false"><i class="mdi mdi-relative-scale"></i> <span class="hide-menu">Tanggung Jawab Sosial</span></a></li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> 
		<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#career" data-parent="#exampleAccordion"><i class="mdi mdi-account-key"></i> <span class="hide-menu">Karir</span></a>
		  <ul class="sidenav-second-level collapse" id="career">
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datacareer" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Info Karir</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datalowongan" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Peluang Kerja</span></a></li>
		  </ul>
        </li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> 
		<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#investor" data-parent="#exampleAccordion"><i class="mdi mdi-account-key"></i> <span class="hide-menu">Investor</span></a>
		  <ul class="sidenav-second-level collapse" id="investor">
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datainvestor" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Update Investor</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datakeuangan" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Data Keuangan</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datarups" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Data RUPS</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datarilis" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Rilis Berita</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datasaham" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Info Saham</span></a></li>
		  </ul>
        </li>
		
		
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> 
		<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#media" data-parent="#exampleAccordion"><i class="mdi mdi-account-key"></i> <span class="hide-menu">Media Publik</span></a>
		  <ul class="sidenav-second-level collapse" id="media">
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datagaleri" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Galeri</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>datapublikasi" aria-expanded="false"><i class="mdi mdi-border-inside"></i> <span class="hide-menu">Berita</span></a></li>
		  </ul>
        </li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> 
		<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion"><i class="mdi mdi-account-key"></i> <span class="hide-menu">administrasi</span></a>
		  <ul class="sidenav-second-level collapse" id="collapseComponents">
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url() ?>login/dataprofil/<?php echo $this->session->userdata("idU"); ?>" aria-expanded="false"><i class="mdi mdi-relative-scale"></i> <span class="hide-menu">Profil Admin</span></a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title=""> <a class="nav-link" href="<?php echo base_url('login/logout'); ?>" aria-expanded="false"><i class="mdi mdi-relative-scale"></i> <span class="hide-menu">Logout</span></a></li>
		  </ul>
        </li>
      </ul>
	<!-- End Navigation--> 

      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="mdi mdi-account"></i> Logout</a>
        </li>
      </ul>
    </div>
  </nav>