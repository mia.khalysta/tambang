	<!-- full Title -->
	<div class="full-title">
		<div class="container">
			<!-- Page Heading/Breadcrumbs -->
			<h1 class="mt-4 mb-3"> </h1>
			<div class="breadcrumb-main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url() ?>index">Home</a>
					</li>
					<li class="breadcrumb-item active">Kontak Kami</li>
				</ol>
			</div>
		</div>
	</div>
	<!-- Page Content -->
    <div class="container" style="margin-top: 30px;">
		<div class="pricing-box">
		<!-- Content Row -->
			<div class="row">
				<div class="col-md-6 mb-4">

						<img class="img-fluid" src="<?php echo base_url() ?>web/images/kantor1.png" alt="" style="width: 20rem;" />

					
				</div>
				<div class="col-md-6">
					<h6>
						<b>Alamat</b> <br>
						Jl. Sutera, KecamataSukadana, Kabupaten Kayong Utara, Kalimantan Barat, Kode Pos 78852.
					</h6><br /><br />

					<table width="100%">
						<tr>
							<td align="center">
							<b>Telepon/SMS </b> <br>
								<a href="#"><span class="fa fa-mobile fa-4x"></span> <br /><br /> 081345402640</a>
							</td>
							<td align="center">
							<b>Email  </b> <br>
								<a href="#"><span class="fa fa-envelope fa-4x"></span> <br /><br /> kantorcamatskd@gmail.com  </a>
							</td>
						</tr>
					</table>
				</div>
				</div>
			<hr>
			</div>
		<!-- /.row -->
		</div>
    </div>
    <!-- /.container -->