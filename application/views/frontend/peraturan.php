	<!-- full Title -->
	<div class="full-title">
		<div class="container">
			<!-- Page Heading/Breadcrumbs -->
			<h1 class="mt-4 mb-3"></h1>
			<div class="breadcrumb-main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url() ?>index">Home</a>
					</li>
					<li class="breadcrumb-item active">Dokumen Terkait Kecamatan Sukadana</li>
				</ol>
			</div>
		</div>
	</div>
	
	<!-- Page Content -->
    <div class="container" style="margin-top: 30px;">
		<div class="pricing-box">
			<div class="table-responsive">
				<table id="zero_config" class="table table-striped table-bordered">
					<thead>
						<tr>
						<th>No</th>
						<th>Dokumen</th>
						<th>Deskripsi</th>
						<th>Lampiran</th>
						</tr>
					</thead>
														
					<tbody>
					<?php
					$no = 1;
					foreach ($peraturan as $hasil) {
					?>
						<tr>
						<td><?php echo $no++ ?></td>
						<td><?php echo $hasil->nama_regulasi ?></td>
						<td><?php echo $hasil->ket ?></td>
						<td><a href="<?php echo base_url() . "files/dokumen/" . $hasil->lampiran ?>" target="_blank"><?php echo $hasil->lampiran ?></a></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	<hr>													
	</div>
