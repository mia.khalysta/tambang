
<!-- partial:index.partial.html -->
<style>
	.ser-first-grid span {
		font-size: 40px;
		color: #00c4da;
		border: 1px solid #eaeaea;
		border-radius: 60px;
		width: 95px;
		height: 95px;
		text-align: center;
		box-shadow: 1px 1px 1px #00c4da;


		transition: 0.5s all;
		transition-property: all;
		transition-duration: 0.5s;
		transition-timing-function: initial;
		transition-delay: initial;
		-webkit-transition: 0.5s all;
		-moz-transition: 0.5s all;
		-o-transition: 0.5s all;
		-ms-transition: 0.5s all;
	}

	.ser-first-grid span:hover {
		color: #fff;
		background: #d40308;
		border: 3px solid #d40308;
		-webkit-box-shadow: 2px 2px 2px #999;
		box-shadow: 4px 3px 4px #777;
		-moz-box-shadow: 2px 2px 2px #999;

		transition: 0.5s all;
		transition-property: all;
		transition-duration: 0.5s;
		transition-timing-function: initial;
		transition-delay: initial;
		-webkit-transition: 0.5s all;
		-moz-transition: 0.5s all;
		-o-transition: 0.5s all;
		-ms-transition: 0.5s all;
	}

	.banner_bottom,
	.mid_slider,
	.tabs_section {
		background-color: rgb(0, 196, 218);
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		background-attachment: fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: 100% 100%;
		width: 100%;

		border-top: 1px solid #ddd;
		border-bottom: 1px solid #ddd;
		box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
	}

	@media(max-width:1366px) {
		.w3l_find-soulmate h3 {
			font-size: 38px;
		}

		.w3l_find-soulmate {
			padding: 55px 0;
		}

		.w3_soulgrid h3 {
			font-size: 22px;
		}

		.location h3 {
			font-size: 38px;
		}

		.w3layous-story {
			padding: 35px 0;
		}

		.app {
			padding: 60px 0px 0 0;
		}

		.agileits_services h2 {
			font-size: 38px;
		}

		.wthree_services-bottom p {
			font-size: 26px;
		}

		.w3layouts-browse h3 {
			font-size: 28px;
			margin-bottom: 40px;
		}

		.w3_C_headquarters {
			padding: 25px 0;
		}

		.w3_C_headquarters h4 {
			margin: 0 30px 10px;
		}

		h2.last-updated {
			font-size: 25px;
		}

		.wthree-help h3 {
			font-size: 33px;
		}

		h2.w3-head {
			font-size: 28px;
		}

		.wthree-help {
			width: 85%;
			margin: 55px auto 65px;
		}

		.w3layouts-inner-banner {
			min-height: 280px;
		}

		.w3layouts_details h4 {
			font-size: 20px;
		}

		.feature-main {
			width: 827px;
			height: 782px;
		}

		.feature {
			padding: 7em 0px 0 0;
		}

		.est-bottom p {
			font-size: 2em;
		}

		.est-top {
			margin-top: 18%;
		}

		.banner-bottom {
			padding: 2em 2em;
		}

		.feature-right img {
			width: 81%;
		}
	}

	.w3l_find-soulmate {
		padding: 60px 0;
		background-color: rgb(0, 196, 218);
	}

	body a {
		transition: 0.5s all;
		-webkit-transition: 0.5s all;
		-o-transition: 0.5s all;
		-moz-transition: 0.5s all;
		-ms-transition: 0.5s all;
	}

	.w3_soulgrid {
		border: 10px solid #00c4da;
		padding: 30px 15px;
		margin: 0px;
		background-color: #FFF;
	}

	.w3_soulgrid i {
		font-size: 40px;
		color: #00c4da;
		border: 1px solid #eaeaea;
		padding: 28px 0;
		border-radius: 60px;
		width: 95px;
		height: 95px;
		text-align: center;
		box-shadow: 1px 1px 1px #00c4da;
	}

	.fa {
		display: inline-block;
		font: normal normal normal 14px/1 FontAwesome;
		font-size: inherit;
		text-rendering: auto;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
	}

	@media (max-width: 1366px) {
		.w3_soulgrid h3 {
			font-size: 22px;
		}
	}

	@media (max-width: 1366px) {
		.w3l_find-soulmate h3 {
			font-size: 38px;
		}
	}

	.w3_soulgrid h3 {
		font-size: 20px;
		color: #ff8a34;
		margin: 30px 0 0px 0;
		text-transform: uppercase;
	}

	h3.tittle {
		color: #f8f9fa;
	}
</style>

<header>

      	<div id="slider" class="nivoSlider">
            <a href="#"><img src="<?php echo base_url() ?>files/slide/2.jpg" alt="slide 1" /></a>
            <a href="#"><img src="<?php echo base_url() ?>files/slide/4.jpg" alt="slide 2" /></a>
            <a href="#"><img src="<?php echo base_url() ?>files/slide/3.jpg" alt="slide 3" /></a>
    	</div>
    </header>

	<div class="services-bar">
		<div class="container">
			<h1 class="py-4">Our Best Services </h1>
			<!-- Services Section -->
			<div class="row">
			   <div class="col-lg-6 mb-6">
						<div class="post-slide">
							<div class="pic">
								<img src="<?php echo base_url() ?>files/a.jpg" alt="">
								<ul class="post-category">
									<li><a href="#">Port Facilities</a></li>

								</ul>
							</div>
						</div>
			   </div>
			   <div class="col-lg-6 mb-6">
						<div class="post-slide">
							<div class="pic">
								<img src="<?php echo base_url() ?>files/b.jpg" alt="">
								<ul class="post-category">
									<li><a href="#">Investor</a></li>

								</ul>
							</div>
						</div>
			   </div>
			   
			</div>
			<!-- /.row -->
		</div>
	</div>
		
	
	
	<div class="container">
        <!-- Portfolio Section -->
        <div class="portfolio-main">
            <h2>Insights</h2>

            <div id="projects" class="projects-main row">
               <div class="col-lg-4 col-sm-6 pro-item portfolio-item financial">
                  <div class="card h-100">
                     <div class="card-img">
                        <a href="<?php echo base_url() ?>zonebiz/images/portfolio-img-01.jpg" data-fancybox="images">
                           <img class="card-img-top" src="<?php echo base_url() ?>zonebiz/images/portfolio-img-01.jpg" alt="" />
                           <div class="overlay"><i class="fas fa-arrows-alt"></i></div>
                        </a>
                     </div>
                     <div class="card-body">
                        <h4 class="card-title">
                           <a href="#">Financial Sustainability</a>
                        </h4>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-sm-6 pro-item portfolio-item business academic">
                  <div class="card h-100">
                     <div class="card-img">
                        <a href="<?php echo base_url() ?>zonebiz/images/portfolio-img-02.jpg" data-fancybox="images">
                           <img class="card-img-top" src="<?php echo base_url() ?>zonebiz/images/portfolio-img-02.jpg" alt="" />
                           <div class="overlay"><i class="fas fa-arrows-alt"></i></div>
                        </a>
                     </div>
                     <div class="card-body">
                        <h4 class="card-title">
                           <a href="#">Financial Sustainability</a>
                        </h4>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-sm-6 pro-item portfolio-item travel">
                  <div class="card h-100">
                     <div class="card-img">
                        <a href="<?php echo base_url() ?>zonebiz/images/portfolio-img-03.jpg" data-fancybox="images">
                           <img class="card-img-top" src="<?php echo base_url() ?>zonebiz/images/portfolio-img-03.jpg" alt="" />
                           <div class="overlay"><i class="fas fa-arrows-alt"></i></div>
                        </a>
                     </div>
                     <div class="card-body">
                        <h4 class="card-title">
                           <a href="#">Financial Sustainability</a>
                        </h4>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-sm-6 pro-item portfolio-item business">
                  <div class="card h-100">
                     <div class="card-img">
                        <a href="<?php echo base_url() ?>zonebiz/images/portfolio-img-04.jpg" data-fancybox="images">
                           <img class="card-img-top" src="<?php echo base_url() ?>zonebiz/images/portfolio-img-04.jpg" alt="" />
                           <div class="overlay"><i class="fas fa-arrows-alt"></i></div>
                        </a>
                     </div>
                     <div class="card-body">
                        <h4 class="card-title">
                           <a href="#">Financial Sustainability</a>
                        </h4>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-sm-6 pro-item portfolio-item travel">
                  <div class="card h-100">
                     <div class="card-img">
                        <a href="<?php echo base_url() ?>zonebiz/images/portfolio-img-05.jpg" data-fancybox="images">
                           <img class="card-img-top" src="<?php echo base_url() ?>zonebiz/images/portfolio-img-05.jpg" alt="" />
                           <div class="overlay"><i class="fas fa-arrows-alt"></i></div>
                        </a>
                     </div>
                     <div class="card-body">
                        <h4 class="card-title">
                           <a href="#">Financial Sustainability</a>
                        </h4>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-sm-6 pro-item portfolio-item financial academic">
                  <div class="card h-100">
                     <div class="card-img">
                        <a href="<?php echo base_url() ?>zonebiz/images/portfolio-img-01.jpg" data-fancybox="images">
                           <img class="card-img-top" src="<?php echo base_url() ?>zonebiz/images/portfolio-img-01.jpg" alt="" />
                           <div class="overlay"><i class="fas fa-arrows-alt"></i></div>
                        </a>
                     </div>
                     <div class="card-body">
                        <h4 class="card-title">
                           <a href="#">Financial Sustainability</a>
                        </h4>
                     </div>
                  </div>
               </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
	<div class="blog-slide">
		<div class="container">
			<h2>News & Update</h2>
			<div class="row">
				<div class="col-lg-12">
					<div id="blog-slider" class="owl-carousel">
						<div class="post-slide">
							<div class="post-header">
								<h4 class="title">
									<a href="#">Latest blog Post</a>
								</h4>
								<ul class="post-bar">
									<li><img src="<?php echo base_url() ?>zonebiz/images/testi_01.png" alt=""><a href="#">Williamson</a></li>
									<li><i class="fa fa-calendar"></i>02 June 2018</li>
								</ul>
							</div>
							<div class="pic">
								<img src="<?php echo base_url() ?>zonebiz/images/img-1.jpg" alt="">
								<ul class="post-category">
									<li><a href="#">Business</a></li>
									<li><a href="#">Financ</a></li>
								</ul>
							</div>
							<p class="post-description">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas gravida nulla eu massa efficitur, eu hendrerit ipsum efficitur. Morbi vitae velit ac.
							</p>
						</div>
		 
						<div class="post-slide">
							<div class="post-header">
								<h4 class="title">
									<a href="#">Latest blog Post</a>
								</h4>
								<ul class="post-bar">
									<li><img src="<?php echo base_url() ?>zonebiz/images/testi_02.png" alt=""><a href="#">Kristiana</a></li>
									<li><i class="fa fa-calendar"></i>05 June 2018</li>
								</ul>
							</div>
							<div class="pic">
								<img src="<?php echo base_url() ?>zonebiz/images/img-2.jpg" alt="">
								<ul class="post-category">
									<li><a href="#">Business</a></li>
									<li><a href="#">Financ</a></li>
								</ul>
							</div>
							<p class="post-description">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas gravida nulla eu massa efficitur, eu hendrerit ipsum efficitur. Morbi vitae velit ac.
							</p>
						</div>
						
						<div class="post-slide">
							<div class="post-header">
								<h4 class="title">
									<a href="#">Latest blog Post</a>
								</h4>
								<ul class="post-bar">
									<li><img src="<?php echo base_url() ?>zonebiz/images/testi_01.png" alt=""><a href="#">Kristiana</a></li>
									<li><i class="fa fa-calendar"></i>05 June 2018</li>
								</ul>
							</div>
							<div class="pic">
								<img src="<?php echo base_url() ?>zonebiz/images/img-3.jpg" alt="">
								<ul class="post-category">
									<li><a href="#">Business</a></li>
									<li><a href="#">Financ</a></li>
								</ul>
							</div>
							<p class="post-description">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas gravida nulla eu massa efficitur, eu hendrerit ipsum efficitur. Morbi vitae velit ac.
							</p>
						</div>
						
						<div class="post-slide">
							<div class="post-header">
								<h4 class="title">
									<a href="#">Latest blog Post</a>
								</h4>
								<ul class="post-bar">
									<li><img src="<?php echo base_url() ?>zonebiz/images/testi_02.png" alt=""><a href="#">Kristiana</a></li>
									<li><i class="fa fa-calendar"></i>05 June 2018</li>
								</ul>
							</div>
							<div class="pic">
								<img src="<?php echo base_url() ?>zonebiz/images/img-4.jpg" alt="">
								<ul class="post-category">
									<li><a href="#">Business</a></li>
									<li><a href="#">Financ</a></li>
								</ul>
							</div>
							<p class="post-description">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas gravida nulla eu massa efficitur, eu hendrerit ipsum efficitur. Morbi vitae velit ac.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
