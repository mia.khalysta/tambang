
	<!-- full Title -->
	<div class="full-title">
		<div class="container">
			<!-- Page Heading/Breadcrumbs -->
			<h1 class="mt-4 mb-3"></h1>
			<div class="breadcrumb-main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="index.html">Home</a>
					</li>
					<li class="breadcrumb-item active"> Profil Dewan</li>
				</ol>
			</div>
		</div>
	</div>



    <!-- Page Content -->
	
    <div class="single-services">
	<hr>
      <div class="container" style="max-width: 96%;">
        <div class="row" id="tabs">
          <div class="col-md-4">
            <ul>
              <li><a href='#1'>Dewan Komisaris<i class="fa fa-angle-right"></i></a></li>
			  <li><a href='#2'>Dewan Direksi<i class="fa fa-angle-right"></i></a></li>
			  <li><a href='#3'>Dewan Komite<i class="fa fa-angle-right"></i></a></li>
            </ul>
          </div>
          <div class="col-md-8">
            <section class='tabs-content'>
			
              <article id='1' >
				<!-- Page Komisaris -->
				<div class="container">
					<div class="container">
						<div class="accordion" id="accordionExample">				
						<?php
						$no = 1;
						foreach ($data_komisaris as $hasil) {
						?>
							<div class="card accordion-single">
								<div class="card-header">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#c4<?php echo $hasil->id_dewan ?>" aria-expanded="false" aria-controls="collapseThree">
											<table width="100%">
											  <tr>
												<td  width="400px"><div class="container"><img class="img-fluid rounded" style="width:46%;" src="<?php echo base_url() . "files/dewan/" . $hasil->gambar ?>" alt="" /></div></td>
												<td><div class="container"><?php echo $hasil->nama ?> <br/><?php echo $hasil->jabatan ?></div></td>
											  </tr>
											</table>								
										</button>
									</h5>
								</div>
								<div id="c4<?php echo $hasil->id_dewan ?>" class="collapse" data-parent="#accordionExample">
									<div class="card-body">
										<p><?php echo $hasil->ket ?></p>
									</div>
								</div>
							</div>
						<?php
						}
						?>
						</div>
					</div>
				</div>
				<!-- /.container -->
              </article>
			  <article id='2' >
				<!-- Page Direksi -->
				<div class="faq-main">
					<div class="container">
						<div class="accordion" id="accordionExample">				
						<?php
						$no = 1;
						foreach ($data_direksi as $hasil) {
						?>
							<div class="card accordion-single">
								<div class="card-header">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#c4<?php echo $hasil->id_direksi ?>" aria-expanded="false" aria-controls="collapseThree">
											<table width="100%">
											  <tr>
												<td  width="400px"><div class="container"><img class="img-fluid rounded" style="width:46%;" src="<?php echo base_url() . "files/direksi/" . $hasil->gambar ?>" alt="" /></div></td>
												<td><div class="container"><?php echo $hasil->nama ?> <br/><?php echo $hasil->jabatan ?></div></td>
											  </tr>
											</table>								
											</button>
										</h5>
									</div>
									<div id="c4<?php echo $hasil->id_direksi ?>" class="collapse" data-parent="#accordionExample">
										<div class="card-body">
											<p><?php echo $hasil->ket ?></p>
										</div>
									</div>
							</div>
							<?php
							}
							?>
						</div>
					</div>
				</div>
				<!-- /.container -->
              </article>
			  <article id='3' >
				<!-- Page Komite -->
				<div class="faq-main">
					<div class="container">
						<div class="accordion" id="accordionExample">				
						<?php
						$no = 1;
						foreach ($data_komite as $hasil) {
						?>
							<div class="card accordion-single">
								<div class="card-header">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#c4<?php echo $hasil->id_komite ?>" aria-expanded="false" aria-controls="collapseThree">				
											<table width="100%">
											  <tr>
												<td  width="400px"><div class="container"><img class="img-fluid rounded" style="width:46%;" src="<?php echo base_url() . "files/komite/" . $hasil->gambar ?>" alt="" /></div></td>
												<td><div class="container"><?php echo $hasil->nama ?> <br/><?php echo $hasil->jabatan ?></div></td>
											  </tr>
											</table>								
										</button>
									</h5>
								</div>
								<div id="c4<?php echo $hasil->id_komite ?>" class="collapse" data-parent="#accordionExample">
									<div class="card-body">
										<p><?php echo $hasil->ket ?></p>
									</div>
								</div>
							</div>
						<?php
						}
						?>
						</div>
					</div>
				</div>
				<!-- /.container -->
              </article>
            </section>
          </div>
        </div>
      </div>
	  <hr>
    </div>
	
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url() ?>finance/vendor/jquery/jquery.min.js"></script>

    <!-- Additional Scripts -->
    <script src="<?php echo base_url() ?>finance/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>finance/assets/js/owl.js"></script>
    <script src="<?php echo base_url() ?>finance/assets/js/slick.js"></script>
    <script src="<?php echo base_url() ?>finance/assets/js/accordions.js"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>