
	<!-- full Title -->
	<div class="full-title">
		<div class="container">
			<!-- Page Heading/Breadcrumbs -->
			<h1 class="mt-4 mb-3"></h1>
			<div class="breadcrumb-main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="index.html">Home</a>
					</li>
					<li class="breadcrumb-item active"> Profil Perusahaan</li>
				</ol>
			</div>
		</div>
	</div>



    <!-- Page Content -->
	
    <div class="single-services">
	<hr>
      <div class="container" style="max-width: 96%;">
        <div class="row" id="tabs">
          <div class="col-md-4">
            <ul>
			<?php
			$no = 1;
			foreach ($data_about as $hasil) {
			?>
              <li><a href='#tabs<?php echo $hasil->id_about ?>'><?php echo $hasil->nama_about ?> <i class="fa fa-angle-right"></i></a></li>
            <?php
            }
			?>
            </ul>
          </div>
          <div class="col-md-8">
            <section class='tabs-content'>
			<?php
			$no = 1;
			foreach ($data_about as $hasil){
			?>
              <article id='tabs<?php echo $hasil->id_about ?>' >
			  <p><?php echo $hasil->keterangan ?></p>
              </article>
            <?php
            }
			?>  
            </section>
          </div>
        </div>
      </div>
	  <hr>
    </div>
	
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url() ?>finance/vendor/jquery/jquery.min.js"></script>


    <!-- Additional Scripts -->
    <script src="<?php echo base_url() ?>finance/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>finance/assets/js/owl.js"></script>
    <script src="<?php echo base_url() ?>finance/assets/js/slick.js"></script>
    <script src="<?php echo base_url() ?>finance/assets/js/accordions.js"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>
