<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title ?></title>
	<meta charset="utf-8">
	<meta name="keywords" content="Demesne a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url() ?>zonebiz/css/bootstrap.min.css" rel="stylesheet">
	<!-- Fontawesome CSS -->
	<link href="<?php echo base_url() ?>zonebiz/css/all.css" rel="stylesheet">
	<!-- Owl Carousel CSS -->
	<link href="<?php echo base_url() ?>zonebiz/css/owl.carousel.min.css" rel="stylesheet">
	<!-- Owl Carousel CSS -->
	<link href="<?php echo base_url() ?>zonebiz/css/jquery.fancybox.min.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="<?php echo base_url() ?>zonebiz/css/style.css" rel="stylesheet">
	
	<!-- Bootstrap -->

    <link href="<?php echo base_url() ?>zonebiz/tt/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>zonebiz/tt/css/templatemo_style.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>zonebiz/css/templatemo-finance-business.css" rel="stylesheet" >
    <link href="<?php echo base_url() ?>zonebiz/tt/css/circle.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>zonebiz/tt/css/jquery.bxslider.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>zonebiz/tt/css/nivo-slider.css" rel="stylesheet" >
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,600' rel='stylesheet' type='text/css'>
    <script src="<?php echo base_url() ?>zonebiz/tt/js/modernizr.custom.js"></script>
	
	
	<link href="<?php echo base_url() ?>assests/css/easy-responsive-tabs.css" rel='stylesheet' type='text/css' />
	
	<link href="<?php echo base_url() ?>assests/css/font-awesome.css" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo base_url() ?>/slider/inset.css">

	<link href="<?php echo base_url() ?>assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/li	bs/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/libs/jquery-minicolors/jquery.minicolors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<style>
	.fixed-menu {
    padding: .5rem 1rem;
	}
	.navbar-expand-lg.top-nav .navbar-nav .nav-link {
    font-size: 13px;
    }
	</style>
	<link href="<?php echo base_url() ?>dist/css/style.min.css" rel="stylesheet">
</head>


<body>
	<!--Header-->
	<div class="header" id="home">
		<!--/top-bar-->
		<div class="top-bar_w3layouts">
		
<style>
.full-title {
    background-size: 100%;
}
.navbar-dark .navbar-nav .active>.nav-link, .navbar-dark .navbar-nav .nav-link.active, .navbar-dark .navbar-nav .nav-link.show, .navbar-dark .navbar-nav .show>.nav-link {
    color: #000;
	font-weight: bold;
}
#accordionExample .accordion-single:before {
    content: "";
    display: block;
    width: 1px;
    height: 100%;
    border: 0px dashed #1273eb;
    position: absolute;
    top: 25px;
    left: 18px;
}
</style>

<!-- Top Bar -->
	<div class="top-bar">
		<div class="container">
			<div class="row">

			</div>
		</div>
	</div>
