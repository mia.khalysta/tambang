<style>
.card-img img {
    width: 100%;
	height: 13rem;
}
</style>
	<!-- full Title -->
	<div class="full-title">
		<div class="container">
			<!-- Page Heading/Breadcrumbs -->
			<h1 class="mt-4 mb-3"> </h1>
			<div class="breadcrumb-main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url() ?>index">Home</a>
					</li>
					<li class="breadcrumb-item active">Galeri Kegiatan</li>
				</ol>
			</div>
		</div>
	</div>

    <div class="blog-main">
		<div class="container">
        <!-- Portfolio Section -->
        <div class="portfolio-main">
            <div id="projects" class="projects-main row">
			<?php
				$no = 1;
				foreach($foto as $hasil){
			?>
               <div class="col-lg-4 col-sm-6 pro-item portfolio-item financial">
                  <div class="card h-100">
				  <p style="color: #000;"><?php echo date("d/M/Y",strtotime($hasil->tanggal)) ?></p>
                     <div class="card-img">
                        <a href="<?php echo base_url() . "files/galeri/" . $hasil->gambar ?>" data-fancybox="images">
                           <img class="card-img-top" src="<?php echo base_url() . "files/galeri/" . $hasil->gambar ?>" alt="" />
                           <div class="overlay"><i class="fas fa-arrows-alt"></i></div>
                        </a>
                     </div>
                     <div class="card-body" style="position: relative;">					 
                        <p class="card-title" style="color: #fff;"><?php echo $hasil->judul ?></p>
                     </div>
                  </div>
               </div>
			<?php
				}
			?>   
            </div>
            <!-- /.row -->
        </div>
    </div>
		<!-- /.container -->
	</div>

<hr>
