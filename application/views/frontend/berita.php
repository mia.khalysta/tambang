
	<!-- full Title -->
	<div class="full-title">
		<div class="container">
			<!-- Page Heading/Breadcrumbs -->
			<h1 class="mt-4 mb-3">  </h1>
			<div class="breadcrumb-main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url() ?>index">Home</a>
					</li>
					<li class="breadcrumb-item active">Kumpulan Berita</li>
				</ol>
			</div>
		</div>
	</div>

    <div class="portfolio-col">
		<div class="container">
			<div class="row">
				<?php
				$no = 1;
				foreach($results as $row){
				?>
				<div class="col-lg-4 col-sm-6 portfolio-item">
					<div class="card h-100">
						<a class="hover-box" href="<?php echo base_url() ?>frontberita/post/<?php echo $row->id_publikasi ?>">
							<div class="dot-full">
								<i class="fas fa-link"></i>
							</div>
							<img class="card-img-top" src="<?php echo base_url() . "files/publikasi/" . $row->gambar ?>" width="100%" alt="" />
						</a>
						<div class="card-body">
							<h4><a href="<?php echo base_url() ?>frontberita/post/<?php echo $row->id_publikasi ?>"><?php echo $row->judul ?></a></h4>
							<p><?php echo date("d/M/Y",strtotime($row->tanggal)) ?></p>
						</div>
					</div>
				</div>
				<?php } ?>	
			</div>
			
			
				
			<div class="pagination_bar">
				<!-- Pagination -->
				<?php
				if(isset($links)){
					echo $links;
				} 
				?>
			</div>
		</div>
		<!-- /.container -->
	</div>
<hr>
<style>
.pagination > li {
  display: inline;
}
.pagination > li > a,
.pagination > li > span {
  position: relative;
  float: left;
  padding: 6px 12px;
  margin-left: -1px;
  line-height: 1.42857143;
  color: #337ab7;
  text-decoration: none;
  background-color: #fff;
  border: 1px solid #ddd;
}
.pagination > li:first-child > a,
.pagination > li:first-child > span {
  margin-left: 0;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.pagination > li:last-child > a,
.pagination > li:last-child > span {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}
.pagination > li > a:hover,
.pagination > li > span:hover,
.pagination > li > a:focus,
.pagination > li > span:focus {
  z-index: 2;
  color: #23527c;
  background-color: #eee;
  border-color: #ddd;
}
.pagination > .active > a,
.pagination > .active > span,
.pagination > .active > a:hover,
.pagination > .active > span:hover,
.pagination > .active > a:focus,
.pagination > .active > span:focus {
  z-index: 3;
  color: #fff;
  cursor: default;
  background-color: #337ab7;
  border-color: #337ab7;
}
.pagination > .disabled > span,
.pagination > .disabled > span:hover,
.pagination > .disabled > span:focus,
.pagination > .disabled > a,
.pagination > .disabled > a:hover,
.pagination > .disabled > a:focus {
  color: #777;
  cursor: not-allowed;
  background-color: #fff;
  border-color: #ddd;
}
.pagination-lg > li > a,
.pagination-lg > li > span {
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
}
.pagination-lg > li:first-child > a,
.pagination-lg > li:first-child > span {
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
}
.pagination-lg > li:last-child > a,
.pagination-lg > li:last-child > span {
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
}
.pagination-sm > li > a,
.pagination-sm > li > span {
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
}
.pagination-sm > li:first-child > a,
.pagination-sm > li:first-child > span {
  border-top-left-radius: 3px;
  border-bottom-left-radius: 3px;
}
.pagination-sm > li:last-child > a,
.pagination-sm > li:last-child > span {
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
}
</style>