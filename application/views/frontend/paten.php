
	<!-- full Title -->
	<div class="full-title">
		<div class="container">
			<!-- Page Heading/Breadcrumbs -->
			<h1 class="mt-4 mb-3"></h1>
			<div class="breadcrumb-main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url() ?>index">Home</a>
					</li>
					<li class="breadcrumb-item active"> Pelayanan Administrasi Terpadu </li>
				</ol>
			</div>
		</div>
	</div>



    <!-- Page Content -->
	
    <div class="single-services">
	<hr>
      <div class="container">
        <div class="row" id="tabs">
          <div class="col-md-4">
            <ul>
			<?php
			$no = 1;
			foreach ($data_lynpublik as $hasil) {
			?>
              <li><a href='#tabs<?php echo $hasil->id_layanan ?>'><?php echo $hasil->nama_layanan ?> <i class="fa fa-angle-right"></i></a></li>
            <?php
            }
			?>
            </ul>
          </div>
          <div class="col-md-8">
            <section class='tabs-content'>
			<?php
			$no = 1;
			foreach ($data_lynpublik as $hasil){
			?>
              <article id='tabs<?php echo $hasil->id_layanan ?>'>
                
                <p><?php echo $hasil->ket ?></p>
              </article>
            <?php
            }
			?>  
            </section>
          </div>
        </div>
      </div>
	  <hr>
    </div>
	
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url() ?>finance/vendor/jquery/jquery.min.js"></script>


    <!-- Additional Scripts -->
    <script src="<?php echo base_url() ?>finance/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>finance/assets/js/owl.js"></script>
    <script src="<?php echo base_url() ?>finance/assets/js/slick.js"></script>
    <script src="<?php echo base_url() ?>finance/assets/js/accordions.js"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>
	
	
	<!--    <div class="blog-main">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<img class="img-fluid rounded" src="images/blog-item-01.jpg" alt="" />
				<hr>
			</div>
			<div class="col-md-4 blog-right-side">
				  <div class="card my-4">
					<h5 class="card-header">Daftar Layanan Publik</h5>
					<div class="card-body">
					<?php
					$no = 1;
					foreach ($data_lynpublik as $hasil) {
                ?>
					  <div class="row">
						<div class="col-lg-12">
						  <ul class="list-unstyled mb-0">
							<li>
							  <a href="<?php echo $hasil->nama_layanan ?>"><?php echo $hasil->nama_layanan ?></a>
							</li>
						  </ul>
						</div>
<style>
.blog-right-side .card .card-body ul li a {
    border-radius: 4.8px;
    padding: 6px;
    background: none;
    color: #1273eb;
	font-weight:bold;
    display: inline-block;
	font-size:0.78rem;
}
a:hover {
    color: #0056b3;
    text-decoration: underline;
}
</style>					
					  </div>
					  <?php
					}
                ?>
					</div>
				  </div>
			  </div>
		  </div>
		</div>
	</div>
 -->
