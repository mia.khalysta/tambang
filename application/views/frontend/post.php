
	<!-- full Title -->
	<div class="full-title">
		<div class="container">
			<!-- Page Heading/Breadcrumbs -->
			<h1 class="mt-4 mb-3"> </h1>
			<div class="breadcrumb-main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url() ?>index">Home</a>
					</li>
					<li class="breadcrumb-item active">Detail Berita</li>
				</ol>
			</div>
		</div>
	</div>

    <div class="blog-main">
		<div class="container">
			<div class="row">
			<!-- Post Content Column -->
				<div class="col-lg-8">
				<input type="hidden" value="<?php echo $data_publikasi->id_publikasi ?>" name="id_publikasi">
					<p><?php echo date("d/M/Y",strtotime($data_publikasi->tanggal)) ?></p>
					<hr>
					<!-- Preview Image -->
					<img class="img-fluid rounded" src="<?php echo base_url() . "files/publikasi/" . $data_publikasi->gambar ?>" width="100%" alt="" />
					<hr>
						<!-- Date/Time -->
						
					<!-- Post Content -->
					<blockquote class="blockquote lead">
						<p class="mb-0"><?php echo $data_publikasi->judul ?></p>
					</blockquote>
					<p><?= $data_publikasi->ket ?></p>

				<hr>
				</div>

				<!-- Sidebar Widgets Column -->
				<div class="col-md-4 blog-right-side">

				  <!-- Side Widget -->
				  <div class="card my-4">
					<h5 class="card-header">Berita Terkini</h5>
					<div class="card-body">

						<?php
							$no = 1;
							foreach ($berita as $hasil) {
						?>

						  <ul class="list-unstyled mb-0">
						  <a class="hover-box" href="<?php echo base_url() ?>frontberita/post/<?php echo $hasil->id_publikasi ?>">
							<li>
							  <p><?php echo date("d/M/Y",strtotime($hasil->tanggal)) ?></p>
							</li>
							<li>
							  <p class="mb-0" style="color: #212529;font-weight: bold;"><?php echo $hasil->judul ?></p>
							</li>
							</a>
						  </ul>
						<hr>  

						<?php
							}
						?>
					</div>
				  </div>
				</div>

			</div>
		  <!-- /.row -->
		</div>
		<!-- /.container -->
	</div>


