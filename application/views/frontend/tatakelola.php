
	<!-- full Title -->
	<div class="full-title">
		<div class="container">
			<!-- Page Heading/Breadcrumbs -->
			<h1 class="mt-4 mb-3"></h1>
			<div class="breadcrumb-main">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="index.html">Home</a>
					</li>
					<li class="breadcrumb-item active"> Tata Kelola</li>
				</ol>
			</div>
		</div>
	</div>
<!-- Page Content -->
    <div class="container" style="margin-top: 30px;">
		<div class="pricing-box">
		<!-- Content Row -->
			<div class="row">
				<div class="col-lg-12 mb-12">
				<?php
				$no = 1;
				foreach ($data_kelola as $hasil) {
				?>
				<div class="container"><?php echo $hasil->judul ?></div><br/>						
				<div class="container"><?php echo $hasil->ket ?></div>
				<div class="container"><img class="img-fluid rounded" style="width:100%;" src="<?php echo base_url() . "files/kelola/" . $hasil->gambar ?>" alt="" /></div>
				<?php
				}
                ?>	
				</div>				
			</div>
		<!-- /.row -->
		</div>
    </div>
    <!-- /.container -->

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url() ?>finance/vendor/jquery/jquery.min.js"></script>
    <!-- Additional Scripts -->
    <script src="<?php echo base_url() ?>finance/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>finance/assets/js/owl.js"></script>
    <script src="<?php echo base_url() ?>finance/assets/js/slick.js"></script>
    <script src="<?php echo base_url() ?>finance/assets/js/accordions.js"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>