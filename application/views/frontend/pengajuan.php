	<link href="<?php echo base_url() ?>assests/css/blog.css" rel="stylesheet" type='text/css' media="all" />
	<link href="<?php echo base_url() ?>assests/css/single.css" rel="stylesheet" type='text/css' media="all" />

	<div class="banner_bottom" style="padding-top: 0px;">
		<div class="container">
			<div class="inner_sec_info_w3ls_agile">
				<div class="col-md-12 blog_section">
					<div class="tittle_head" style="padding: 20px;">
						<h3>FORM USULAN BANTUAN</h3>
					</div>
					<div class="blog_img single">
						<div class="contact-form left_bar" style="background:#fff;margin-top: 0em;">
							<? php // echo form_open_multipart('usulan/simpan') 
							?>
							<?php echo form_open_multipart() ?>
							<div class="left_form">
								<div class="form-group">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<label for="text">NIK</label>
									<input type="text" name="TxtNIK" class="form-control" placeholder="Masukkan NIK Anda" required autofocus value="<?= set_value('TxtNIK'); ?>">
									<?= form_error('TxtNIK', '<small class="text-danger">', '</small>'); ?>
								</div>

								<div class="form-group">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<label for="text">Jenis Bantuan</label>
									<select class="form-control" name="CmbJnisBntuan">
										<option disabled selected>--Pilih jenis bantuan--</option>
										<?php
										foreach ($data_jnsbntuan as $DtJenis) {
											?>
											<option value="<?= $DtJenis->id_jenis ?>"><?= $DtJenis->nama_jenis ?></option>
										<?php
										}
										?>
									</select>
									<?= form_error('CmbJnisBntuan', '<small class="text-danger">', '</small>'); ?>
								</div>
								<div class="form-group">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<label for="text">Keterangan</label>
									<textarea name="TxtKet" rows="3" class="form-control" required><?= set_value('TxtKet'); ?></textarea>
									<?= form_error('TxtKet', '<small class="text-danger">', '</small>'); ?>
								</div>
							</div>

							<div class="right_form">
								<div class="form-group">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<label for="text">Nomor Surat</label>
									<input type="text" name="TxtNoSurat" class="form-control" required value="<?= set_value('TxtNoSurat'); ?>">
									<?= form_error('TxtNoSurat', '<small class="text-danger">', '</small>'); ?>
								</div>
								<div class="form-group">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<label for="text">Tanggal</label>
									<input type="date" name="TxtTanggal" class="form-control" value="<?= set_value('TxtTanggal'); ?>">
									<?= form_error('TxtTanggal', '<small class="text-danger">', '</small>'); ?>
								</div>
								<div class="form-group">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<label for="text">Perihal</label>
									<input type="text" name="TxtPerihal" class="form-control" required value="<?= set_value('TxtPerihal'); ?>">
									<?= form_error('TxtPerihal', '<small class="text-danger">', '</small>'); ?>
								</div>

								<div class="form-group">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<label for="text">Dokumen</label>
									<input type="file" name="berkas" class="form-control" accept="application/vnd.rar, application/zip, image/*, .doc, .docx, .pdf" required style="
    padding-top: 3px;
    padding-bottom: 3px;
">
									<?= form_error('berkas', '<small class="text-danger">', '</small>'); ?>
								</div>
								<button type="submit" class="myButton">Kirim</button>
								<?php echo form_close() ?>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>