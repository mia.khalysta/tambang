<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_komite extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_komite')
      ->order_by('id_komite', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_komite", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_komite)
  {
    $query = $this->db->where("id_komite", $id_komite)
      ->get("tbl_komite");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_komite", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_komite)
  {
    $this->db->where('id_komite', $id_komite);
    return $this->db->get('tbl_komite');
  }

  public function hapus($id_komite)
  {
    $query = $this->db->delete("tbl_komite", $id_komite);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
