<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_investor extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_investor')
      ->order_by('id_investor', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_investor", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_investor)
  {
    $query = $this->db->where("id_investor", $id_investor)
      ->get("tbl_investor");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_investor", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_investor)
  {
    $this->db->where('id_investor', $id_investor);
    return $this->db->get('tbl_investor');
  }

  public function hapus($id_investor)
  {
    $query = $this->db->delete("tbl_investor", $id_investor);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
