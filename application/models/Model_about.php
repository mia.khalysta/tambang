<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_about extends CI_model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_about')
            ->order_by('id_about', 'ASC')
            ->get();
        return $query->result();
    }

    public function GetDataGambarById($idl)
    {
        $this->db->where('id_about', $idl);
        return $this->db->get('tbl_about');
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_about", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($idp)
    {
        $query = $this->db->where("id_about", $idp)
            ->get("tbl_about");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function update($data, $id)
    {
        $query = $this->db->update("tbl_about", $data, $id);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapus($idp)
    {
        $query = $this->db->delete("tbl_about", $idp);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
} // END OF class Model_kelurahan
