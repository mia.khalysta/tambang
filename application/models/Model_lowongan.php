<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_lowongan extends CI_model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_lowongan')
            ->order_by('id_lowongan', 'DESC')
            ->get();
        return $query->result();
    }

    public function GetDataGambarById($idl)
    {
        $this->db->where('id_lowongan', $idl);
        return $this->db->get('tbl_lowongan');
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_lowongan", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($idp)
    {
        $query = $this->db->where("id_lowongan", $idp)
            ->get("tbl_lowongan");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function update($data, $id)
    {
        $query = $this->db->update("tbl_lowongan", $data, $id);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapus($idp)
    {
        $query = $this->db->delete("tbl_lowongan", $idp);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
} // END OF class Model_kelurahan
