<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_keuangan extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_keuangan')
      ->order_by('id_keuangan', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_keuangan", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_keuangan)
  {
    $query = $this->db->where("id_keuangan", $id_keuangan)
      ->get("tbl_keuangan");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_keuangan", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_keuangan)
  {
    $this->db->where('id_keuangan', $id_keuangan);
    return $this->db->get('tbl_keuangan');
  }

  public function hapus($id_keuangan)
  {
    $query = $this->db->delete("tbl_keuangan", $id_keuangan);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
