<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_dermaga extends CI_model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_dermaga')
            ->order_by('id_dermaga', 'DESC')
            ->get();
        return $query->result();
    }

    public function GetDataGambarById($idl)
    {
        $this->db->where('id_dermaga', $idl);
        return $this->db->get('tbl_dermaga');
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_dermaga", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($idp)
    {
        $query = $this->db->where("id_dermaga", $idp)
            ->get("tbl_dermaga");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function update($data, $id)
    {
        $query = $this->db->update("tbl_dermaga", $data, $id);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapus($idp)
    {
        $query = $this->db->delete("tbl_dermaga", $idp);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
} // END OF class Model_kelurahan
