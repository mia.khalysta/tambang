<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_saham extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_saham')
      ->order_by('id_saham', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_saham", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_saham)
  {
    $query = $this->db->where("id_saham", $id_saham)
      ->get("tbl_saham");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_saham", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_saham)
  {
    $this->db->where('id_saham', $id_saham);
    return $this->db->get('tbl_saham');
  }

  public function hapus($id_saham)
  {
    $query = $this->db->delete("tbl_saham", $id_saham);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
