<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_dewan extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_dewan')
      ->order_by('id_dewan', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_dewan", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_dewan)
  {
    $query = $this->db->where("id_dewan", $id_dewan)
      ->get("tbl_dewan");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_dewan", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_dewan)
  {
    $this->db->where('id_dewan', $id_dewan);
    return $this->db->get('tbl_dewan');
  }

  public function hapus($id_dewan)
  {
    $query = $this->db->delete("tbl_dewan", $id_dewan);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
