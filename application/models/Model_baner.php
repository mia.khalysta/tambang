<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_baner extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_baner')
      ->order_by('id_baner', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_baner", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_baner)
  {
    $query = $this->db->where("id_baner", $id_baner)
      ->get("tbl_baner");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_baner", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_baner)
  {
    $this->db->where('id_baner', $id_baner);
    return $this->db->get('tbl_baner');
  }

  public function hapus($id_baner)
  {
    $query = $this->db->delete("tbl_baner", $id_baner);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
