<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_kelola extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_kelola')
      ->order_by('id_kelola', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_kelola", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_kelola)
  {
    $query = $this->db->where("id_kelola", $id_kelola)
      ->get("tbl_kelola");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_kelola", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_kelola)
  {
    $this->db->where('id_kelola', $id_kelola);
    return $this->db->get('tbl_kelola');
  }

  public function hapus($id_kelola)
  {
    $query = $this->db->delete("tbl_kelola", $id_kelola);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
