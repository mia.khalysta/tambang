<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_kelurahan extends CI_model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_kelurahan')
            ->order_by('id_kelurahan', 'ASC')
            ->get();
        return $query->result();
    }

    public function GetDataGambarById($idl)
    {
        $this->db->where('id_kelurahan', $idl);
        return $this->db->get('tbl_kelurahan');
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_kelurahan", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($idp)
    {
        $query = $this->db->where("id_kelurahan", $idp)
            ->get("tbl_kelurahan");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function update($data, $id)
    {
        $query = $this->db->update("tbl_kelurahan", $data, $id);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapus($idp)
    {
        $query = $this->db->delete("tbl_kelurahan", $idp);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
} // END OF class Model_kelurahan
