<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_rilis extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_rilis')
      ->order_by('id_rilis', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_rilis", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_rilis)
  {
    $query = $this->db->where("id_rilis", $id_rilis)
      ->get("tbl_rilis");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_rilis", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_rilis)
  {
    $this->db->where('id_rilis', $id_rilis);
    return $this->db->get('tbl_rilis');
  }

  public function hapus($id_rilis)
  {
    $query = $this->db->delete("tbl_rilis", $id_rilis);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
