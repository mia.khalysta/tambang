<?php

class M_login extends CI_Model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_user')
            ->order_by('id_user', 'DESC')
            ->get();
        return $query->result();
    }

    function cek_login($table, $where)
    {
        return $this->db->get_where($table, $where)->row_array();
    }

    public function getp($username, $password)
    {
        $query = $this->db->where("username", $username)
            ->where("password", $password)
            ->get("tbl_user");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_user", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($idp)
    {
        $query = $this->db->where("id_user", $idp)
            ->get("tbl_user");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function update($data, $id)
    {
        $query = $this->db->where("id_user", $id)
            ->update("tbl_user", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapusu($idu)
    {
        $query = $this->db->delete("tbl_user", $idu);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}
