<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_direksi extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_direksi')
      ->order_by('id_direksi', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_direksi", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_direksi)
  {
    $query = $this->db->where("id_direksi", $id_direksi)
      ->get("tbl_direksi");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_direksi", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_direksi)
  {
    $this->db->where('id_direksi', $id_direksi);
    return $this->db->get('tbl_direksi');
  }

  public function hapus($id_direksi)
  {
    $query = $this->db->delete("tbl_direksi", $id_direksi);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
