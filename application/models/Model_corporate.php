<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_corporate extends CI_model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_corporate')
            ->order_by('id_corporate', 'DESC')
            ->get();
        return $query->result();
    }

    public function GetDataGambarById($idl)
    {
        $this->db->where('id_corporate', $idl);
        return $this->db->get('tbl_corporate');
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_corporate", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($idp)
    {
        $query = $this->db->where("id_corporate", $idp)
            ->get("tbl_corporate");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function update($data, $id)
    {
        $query = $this->db->update("tbl_corporate", $data, $id);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapus($idp)
    {
        $query = $this->db->delete("tbl_corporate", $idp);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
} // END OF class Model_kelurahan
