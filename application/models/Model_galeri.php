<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_galeri extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_galeri')
      ->order_by('id_galeri', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_galeri", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_galeri)
  {
    $query = $this->db->where("id_galeri", $id_galeri)
      ->get("tbl_galeri");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_galeri", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_galeri)
  {
    $this->db->where('id_galeri', $id_galeri);
    return $this->db->get('tbl_galeri');
  }

  public function hapus($id_galeri)
  {
    $query = $this->db->delete("tbl_galeri", $id_galeri);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
