<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_port extends CI_model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_port')
            ->order_by('id_port', 'DESC')
            ->get();
        return $query->result();
    }

    public function GetDataGambarById($idl)
    {
        $this->db->where('id_port', $idl);
        return $this->db->get('tbl_port');
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_port", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($idp)
    {
        $query = $this->db->where("id_port", $idp)
            ->get("tbl_port");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function update($data, $id)
    {
        $query = $this->db->update("tbl_port", $data, $id);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapus($idp)
    {
        $query = $this->db->delete("tbl_port", $idp);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
} // END OF class Model_kelurahan
