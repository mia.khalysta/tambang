<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_rups extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_rups')
      ->order_by('id_rups', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_rups", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_rups)
  {
    $query = $this->db->where("id_rups", $id_rups)
      ->get("tbl_rups");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_rups", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_rups)
  {
    $this->db->where('id_rups', $id_rups);
    return $this->db->get('tbl_rups');
  }

  public function hapus($id_rups)
  {
    $query = $this->db->delete("tbl_rups", $id_rups);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
