<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_penghargaan extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_penghargaan')
      ->order_by('id_penghargaan', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_penghargaan", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_penghargaan)
  {
    $query = $this->db->where("id_penghargaan", $id_penghargaan)
      ->get("tbl_penghargaan");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_penghargaan", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_penghargaan)
  {
    $this->db->where('id_penghargaan', $id_penghargaan);
    return $this->db->get('tbl_penghargaan');
  }

  public function hapus($id_penghargaan)
  {
    $query = $this->db->delete("tbl_penghargaan", $id_penghargaan);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
