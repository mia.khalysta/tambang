<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_mining extends CI_model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_mining')
            ->order_by('id_mining', 'DESC')
            ->get();
        return $query->result();
    }

    public function GetDataGambarById($idl)
    {
        $this->db->where('id_mining', $idl);
        return $this->db->get('tbl_mining');
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_mining", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($idp)
    {
        $query = $this->db->where("id_mining", $idp)
            ->get("tbl_mining");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function update($data, $id)
    {
        $query = $this->db->update("tbl_mining", $data, $id);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapus($idp)
    {
        $query = $this->db->delete("tbl_mining", $idp);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
} // END OF class Model_kelurahan
