<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_career extends CI_model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_career')
            ->order_by('id_career', 'DESC')
            ->get();
        return $query->result();
    }

    public function GetDataGambarById($idl)
    {
        $this->db->where('id_career', $idl);
        return $this->db->get('tbl_career');
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_career", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($idp)
    {
        $query = $this->db->where("id_career", $idp)
            ->get("tbl_career");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function update($data, $id)
    {
        $query = $this->db->update("tbl_career", $data, $id);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapus($idp)
    {
        $query = $this->db->delete("tbl_career", $idp);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
} // END OF class Model_kelurahan
