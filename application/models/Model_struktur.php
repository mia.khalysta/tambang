<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_struktur extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_struktur')
      ->order_by('id_struktur', 'ASC')
      ->get();
    return $query->result();
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_struktur", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_struktur)
  {
    $query = $this->db->where("id_struktur", $id_struktur)
      ->get("tbl_struktur");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_struktur", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_struktur)
  {
    $this->db->where('id_struktur', $id_struktur);
    return $this->db->get('tbl_struktur');
  }

  public function hapus($id_struktur)
  {
    $query = $this->db->delete("tbl_struktur", $id_struktur);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
