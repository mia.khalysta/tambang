<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_organisasi extends CI_Model
{
  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_organisasi')
      ->order_by('id_org', 'DESC')
      ->get();
    return $query->result();
  }

  public function barisOrg($data)
  {
    $query = $this->db->where($data)
      ->get("tbl_organisasi");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function simpan($data)
  {
    $query = $this->db->insert("tbl_organisasi", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_org)
  {
    $query = $this->db->where("id_org", $id_org)
      ->get("tbl_organisasi");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function GetDataGambarById($idb)
  {
    $this->db->where('id_org', $idb);
    return $this->db->get('tbl_organisasi');
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_organisasi", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function hapus($idOrg)
  {
    $query = $this->db->delete("tbl_organisasi", $idOrg);
    $query2 = $this->db->delete("tbl_anggota", $idOrg);

    if ($query and $query2) {
      return true;
    } else {
      return false;
    }
  }
} // END OF CLASS Model_organisasi
