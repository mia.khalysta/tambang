<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_webterkait extends CI_model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_webterkait')
            ->order_by('id_link', 'DESC')
            ->get();
        return $query->result();
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_webterkait", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($id_link)
    {
        $query = $this->db->where("id_link", $id_link)
            ->get("tbl_webterkait");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function update($data, $id)
    {
        $query = $this->db->update("tbl_webterkait", $data, $id);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapus($id)
    {
        $query = $this->db->delete("tbl_webterkait", $id);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
} // END OF class Model_webterkait
