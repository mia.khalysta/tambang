<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_publikasi extends CI_model
{
	
	public function get_current_page($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get('tbl_publikasi');
        $rows = $query->result();
 
        if ($query->num_rows() > 0) {
            foreach ($rows as $row) {
                $data[] = $row;
            }
             
            return $data;
        }
 
        return false;
    }
     
    public function get_total() {
        return $this->db->count_all('tbl_publikasi');
    }
	
  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('tbl_publikasi')
      ->order_by('id_publikasi', 'DESC')
      ->get();
    return $query->result();
  }

  public function limited()
  {
    $this->db->limit(5);
	$query = $this->db->select("*")
      ->from('tbl_publikasi')
      ->order_by('id_publikasi', 'DESC')
      ->get();
    return $query->result();
  }
  
  public function simpan($data)
  {
    $query = $this->db->insert("tbl_publikasi", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function edit($id_publikasi)
  {
    $query = $this->db->where("id_publikasi", $id_publikasi)
      ->get("tbl_publikasi");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }
  
  public function post($id_publikasi)
  {
    $query = $this->db->where("id_publikasi", $id_publikasi)
      ->get("tbl_publikasi");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("tbl_publikasi", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function GetDataGambarById($id_publikasi)
  {
    $this->db->where('id_publikasi', $id_publikasi);
    return $this->db->get('tbl_publikasi');
  }

  public function hapus($id_publikasi)
  {
    $query = $this->db->delete("tbl_publikasi", $id_publikasi);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
}
