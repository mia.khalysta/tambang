<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_lokasi extends CI_model
{
    public function get_all()
    {
        $query = $this->db->select("*")
            ->from('tbl_lokasi')
            ->order_by('id_lokasi', 'DESC')
            ->get();
        return $query->result();
    }

    public function GetDataGambarById($idl)
    {
        $this->db->where('id_lokasi', $idl);
        return $this->db->get('tbl_lokasi');
    }

    public function simpan($data)
    {
        $query = $this->db->insert("tbl_lokasi", $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($idp)
    {
        $query = $this->db->where("id_lokasi", $idp)
            ->get("tbl_lokasi");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function update($data, $id)
    {
        $query = $this->db->update("tbl_lokasi", $data, $id);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function hapus($idp)
    {
        $query = $this->db->delete("tbl_lokasi", $idp);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }
} // END OF class Model_kelurahan
