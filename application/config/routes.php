<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes with
| underscores in the controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['bansos1234adm'] = 'login/index';
$route['administrator'] = 'login/aksi_login';


$route['daftar_bantuan'] = 'Report_bantuan/data_pengajuan';
$route['filter'] = 'Report_bantuan/GetDataPengajuan';

$route['dashboard'] = 'admin/index';

$route['logout'] = 'login/logout';
$route['tambahbaner'] = 'baner/tambah';
$route['databaner'] = 'baner/databaner';
$route['datatitle'] = 'title/datatitle';

$route['datalogo'] = 'logo/datalogo';
$route['databackground'] = 'background/databackground';


$route['datamining'] = 'mining/datamining';
$route['tambahmining'] = 'mining/tambah';
$route['dataport'] = 'port/dataport';
$route['tambahport'] = 'port/tambah';
$route['datacorporate'] = 'corporate/datacorporate';
$route['tambahcorporate'] = 'corporate/tambah';
$route['datacareer'] = 'career/datacareer';
$route['tambahcareer'] = 'career/tambah';

$route['dataabout'] = 'about/dataabout';
$route['tambahabout'] = 'about/tambah';
$route['datakomite'] = 'komite/datakomite';
$route['tambahkomite'] = 'komite/tambah';
$route['datadireksi'] = 'direksi/datadireksi';
$route['tambahdireksi'] = 'direksi/tambah';
$route['datakomisaris'] = 'dewan/datadewan';
$route['tambahkomisaris'] = 'dewan/tambah';
$route['datastruktur'] = 'struktur/datastruktur';
$route['tambahstruktur'] = 'struktur/tambah';
$route['datakelola'] = 'kelola/datakelola';
$route['tambahkelola'] = 'kelola/tambah';
$route['datapenghargaan'] = 'penghargaan/datapenghargaan';
$route['tambahpenghargaan'] = 'penghargaan/tambah';
$route['datadermaga'] = 'dermaga/datadermaga';
$route['tambahdermaga'] = 'dermaga/tambah';
$route['datalokasi'] = 'lokasi/datalokasi';
$route['tambahlokasi'] = 'lokasi/tambah';
$route['datalowongan'] = 'lowongan/datalowongan';
$route['tambahlowongan'] = 'lowongan/tambah';
$route['datainvestor'] = 'investor/datainvestor';
$route['tambahinvestor'] = 'investor/tambah';
$route['datakeuangan'] = 'keuangan/datakeuangan';
$route['tambahkeuangan'] = 'keuangan/tambah';
$route['datarups'] = 'rups/datarups';
$route['tambahrups'] = 'rups/tambah';
$route['datarilis'] = 'rilis/datarilis';
$route['tambahrilis'] = 'rilis/tambah';
$route['datasaham'] = 'saham/datasaham';
$route['tambahsaham'] = 'saham/tambah';
$route['datagaleri'] = 'galeri/datagaleri';
$route['tambahgaleri'] = 'galeri/tambah';
$route['datapublikasi'] = 'publikasi/datapublikasi';
$route['tambahpublikasi'] = 'publikasi/tambah';

//Front End

$route['Home'] = 'home/index';
$route['About'] = 'home/dataabout';
$route['Organisasi'] = 'home/datastruktur';
$route['Komisaris'] = 'home/datakomisaris';
$route['Direksi'] = 'home/datadireksi';
$route['Komite'] = 'home/datakomite';
$route['Dewan'] = 'home/datadewan';
$route['TataKelola'] = 'home/datakelola';
$route['Penghargaan'] = 'home/datapenghargaan';
$route['Error404'] = '';
$route['translate_uri_dashes'] = FALSE;
