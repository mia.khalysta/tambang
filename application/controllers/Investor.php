<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Investor extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_investor');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datainvestor()
  {

    $data = array(
      'title' => 'Data Investor',
      'data_investor' => $this->model_investor->get_all(),
      'isi' => 'backend/investor/data_investor'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Data Investor',
      'data_investor' => $this->model_investor->get_all(),
      'isi' => 'backend/investor/tambah_investor'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'judul' => $this->input->post("TxtNama"),
      'tanggal' => date("Y-m-d"),
      'gambar' => $this->aksi_upload(),
    );

    $this->model_investor->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Investor berhasil di simpan</div>');

    redirect('datainvestor');
    //$this->datainvestor();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/investor/';
    $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_investor)
  {
    $id_investor = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Data Investor',
      'data_investor' => $this->model_investor->edit($id_investor),
      'isi' => 'backend/investor/edit_investor'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_investor] = $this->input->post("id_investor");

    $idb = $this->input->post("id_investor");
    $data = array(
      'judul' => $this->input->post("TxtNama"),
      'tanggal' => date("Y-m-d"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_investor->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datainvestor');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_investor->GetDataGambarById($idb)->row();
    $LetakGambar = './files/investor/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/investor/';
      $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx';

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_investor)
  {
    $id['id_investor'] = $this->uri->segment(3);

    $dataGbr = $this->model_investor->GetDataGambarById($id_investor)->row();
    $LetakGambar = './files/investor/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_investor->hapus($id);
      redirect('investor/datainvestor');
    } else {
      $this->model_investor->hapus($id);
      redirect('investor/datainvestor');
    }
  }
}
