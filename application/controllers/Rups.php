<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rups extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_rups');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datarups()
  {

    $data = array(
      'title' => 'Data RUPS',
      'data_rups' => $this->model_rups->get_all(),
      'isi' => 'backend/rups/data_rups'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Data RUPS',
      'data_rups' => $this->model_rups->get_all(),
      'isi' => 'backend/rups/tambah_rups'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'judul' => $this->input->post("TxtNama"),
	  'tanggal' => date("Y-m-d"),
	  'kategori' => $this->input->post("TxtKategori"),
      'gambar' => $this->aksi_upload()
    );

    $this->model_rups->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data RUPS berhasil di simpan</div>');

    redirect('datarups');
    //$this->datarups();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/rups/';
    $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_rups)
  {
    $id_rups = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Data RUPS',
      'data_rups' => $this->model_rups->edit($id_rups),
	  'kategori' => $this->input->post("TxtKategori"),
      'isi' => 'backend/rups/edit_rups'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_rups] = $this->input->post("id_rups");

    $idb = $this->input->post("id_rups");
    $data = array(
      'judul' => $this->input->post("TxtNama"),
	  'tanggal' => date("Y-m-d"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_rups->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datarups');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_rups->GetDataGambarById($idb)->row();
    $LetakGambar = './files/rups/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/rups/';
      $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx';
      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_rups)
  {
    $id['id_rups'] = $this->uri->segment(3);

    $dataGbr = $this->model_rups->GetDataGambarById($id_rups)->row();
    $LetakGambar = './files/rups/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_rups->hapus($id);
      redirect('rups/datarups');
    } else {
      $this->model_rups->hapus($id);
      redirect('rups/datarups');
    }
  }
}
