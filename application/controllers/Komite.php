<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Komite extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_komite');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datakomite()
  {

    $data = array(
      'title' => 'Profil Komite',
      'data_komite' => $this->model_komite->get_all(),
      'isi' => 'backend/komite/data_komite'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Profil Komite',
      'data_komite' => $this->model_komite->get_all(),
      'isi' => 'backend/komite/tambah_komite'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
	  'komite' => $this->input->post("TxtJudul"),
      'nama' => $this->input->post("TxtNama"),
      'jabatan' => $this->input->post("TxtJabatan"),
      'ket' => $this->input->post("TxtKet"),
      'gambar' => $this->aksi_upload()
    );

    $this->model_komite->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Profil Komite berhasil di simpan</div>');

    redirect('datakomite');
    //$this->datakomite();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/komite/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_komite)
  {
    $id_komite = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Profil Komite',
      'data_komite' => $this->model_komite->edit($id_komite),
      'isi' => 'backend/komite/edit_komite'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_komite] = $this->input->post("id_komite");

    $idb = $this->input->post("id_komite");
    $data = array(
	  'komite' => $this->input->post("TxtJudul"),
      'nama' => $this->input->post("TxtNama"),
      'jabatan' => $this->input->post("TxtJabatan"),
      'ket' => $this->input->post("TxtKet"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_komite->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datakomite');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_komite->GetDataGambarById($idb)->row();
    $LetakGambar = './files/komite/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/komite/';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_komite)
  {
    $id['id_komite'] = $this->uri->segment(3);

    $dataGbr = $this->model_komite->GetDataGambarById($id_komite)->row();
    $LetakGambar = './files/komite/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_komite->hapus($id);
      redirect('komite/datakomite');
    } else {
      $this->model_komite->hapus($id);
      redirect('komite/datakomite');
    }
  }
}
