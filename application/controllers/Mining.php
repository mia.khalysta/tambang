<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mining extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_mining');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datamining()
  {

    $data = array(
      'title' => 'Info Tambang',
      'data_mining' => $this->model_mining->get_all(),
      'isi' => 'backend/mining/data_mining'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Info Tambang',
      'data_mining' => $this->model_mining->get_all(),
      'isi' => 'backend/mining/tambah_mining'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    //Txtmining, TxtKet
    $data = array(
      'nama_mining' => $this->input->post("Txtmining"),
	  'keterangan' => $this->input->post("Txtket")
    );
	
	$nama_mining = $this->input->post('Txtmining');
	$sql = $this->db->query("SELECT nama_mining FROM tbl_mining where nama_mining='$nama_mining'");
	$cek_nik = $sql->num_rows();
	if ($cek_nik > 0) {
	$this->session->set_flashdata('notif', '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Peringatan! Data mining Sudah Ada</div>');
	redirect('tambahmining');
	}else{
    $this->model_mining->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data mining Berhasil di Simpan</div>');

    redirect('datamining');
	}
	
    
  }

  public function edit($idjb)
  {
    $idjb = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Info Tambang',
      'data_mining' => $this->model_mining->edit($idjb),
      'isi' => 'backend/mining/edit_mining'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id['id_mining'] = $this->input->post("TxtIDmining");
	
	$idm = $this->input->post("TxtIDmining");
    $data = array(
      'nama_mining' => $this->input->post("Txtmining"),
	  'keterangan' => $this->input->post("Txtket")
    );

    $this->model_mining->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data mining Berhasil di Update</div>');

    redirect('datamining');
  }

  public function hapus($idj)
  {
    $id['id_mining'] = $this->uri->segment(3);
    $this->model_mining->hapus($id);
    redirect('datamining');
  }

} // END OF class mining
