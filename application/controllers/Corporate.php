<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corporate extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_corporate');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datacorporate()
  {

    $data = array(
      'title' => 'Data Sosial',
      'data_corporate' => $this->model_corporate->get_all(),
      'isi' => 'backend/corporate/data_corporate'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Data Sosial',
      'data_jb' => $this->model_corporate->get_all(),
      'isi' => 'backend/corporate/tambah_corporate'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    //Txtcorporate, TxtKet
    $data = array(
      'nama_corporate' => $this->input->post("Txtcorporate"),
	  'keterangan' => $this->input->post("Txtket")
    );
	
	$nama_corporate = $this->input->post('Txtcorporate');
	$sql = $this->db->query("SELECT nama_corporate FROM tbl_corporate where nama_corporate='$nama_corporate'");
	$cek_nik = $sql->num_rows();
	if ($cek_nik > 0) {
	$this->session->set_flashdata('notif', '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Peringatan! Data corporate Sudah Ada</div>');
	redirect('tambahcorporate');
	}else{
    $this->model_corporate->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data corporate Berhasil di Simpan</div>');

    redirect('datacorporate');
	}
	
    
  }

  public function edit($idjb)
  {
    $idjb = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Data Sosial',
      'data_jb' => $this->model_corporate->edit($idjb),
      'isi' => 'backend/corporate/edit_corporate'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id['id_corporate'] = $this->input->post("TxtIDcorporate");
	
	$idm = $this->input->post("TxtIDcorporate");
    $data = array(
      'nama_corporate' => $this->input->post("Txtcorporate"),
	  'keterangan' => $this->input->post("Txtket")
    );

    $this->model_corporate->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data corporate Berhasil di Update</div>');

    redirect('datacorporate');
  }

  public function hapus($idj)
  {
    $id['id_corporate'] = $this->uri->segment(3);
    $this->model_corporate->hapus($id);
    redirect('datacorporate');
  }

} // END OF class corporate
