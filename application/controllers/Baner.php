<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Baner extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_baner');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function databaner()
  {

    $data = array(
      'title' => 'Data Slideshow',
      'data_baner' => $this->model_baner->get_all(),
      'isi' => 'backend/data_baner'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Data Slideshow',
      'data_baner' => $this->model_baner->get_all(),
      'isi' => 'backend/tambah_baner'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'judul' => $this->input->post("TxtJudul"),
      'urutan' => $this->input->post("TxtUrutan"),
      'gambar' => $this->aksi_upload()
    );

    $this->model_baner->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Slideshow berhasil di simpan</div>');

    redirect('databaner');
    //$this->databaner();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/slide/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_baner)
  {
    $id_baner = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Data Slideshow',
      'data_baner' => $this->model_baner->edit($id_baner),
      'isi' => 'backend/edit_baner'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_baner] = $this->input->post("id_baner");

    $idb = $this->input->post("id_baner");
    $data = array(
      'judul' => $this->input->post("TxtJudul"),
      'urutan' => $this->input->post("TxtUrutan"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_baner->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('databaner');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_baner->GetDataGambarById($idb)->row();
    $LetakGambar = './files/slide/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/slide/';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_baner)
  {
    $id['id_baner'] = $this->uri->segment(3);

    $dataGbr = $this->model_baner->GetDataGambarById($id_baner)->row();
    $LetakGambar = './files/slide/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_baner->hapus($id);
      redirect('baner/databaner');
    } else {
      $this->model_baner->hapus($id);
      redirect('baner/databaner');
    }
  }
}
