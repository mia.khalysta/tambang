<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Galeri extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_galeri');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {
    $this->load->view('index');
  }
   
  public function datagaleri()
  {
    $data = array(
      'title' => 'Data Galeri',
      'data_galeri' => $this->model_galeri->get_all(),
      'isi' => 'backend/data_galeri'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Data Galeri',
      'data_galeri' => $this->model_galeri->get_all(),
      'isi' => 'backend/tambah_galeri'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'judul' => $this->input->post("TxtJudul"),
      'tanggal' => date("Y-m-d"),
      'gambar' => $this->aksi_upload()
    );

    $this->model_galeri->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Galeri berhasil di simpan</div>');

    redirect('datagaleri');
    //$this->datagaleri();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/galeri/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_galeri)
  {
    $id_galeri = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Data Galeri',
      'data_galeri' => $this->model_galeri->edit($id_galeri),
      'isi' => 'backend/edit_galeri'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_galeri] = $this->input->post("id_galeri");

    $idb = $this->input->post("id_galeri");
    $data = array(
      'judul' => $this->input->post("TxtJudul"),
      'tanggal' => date("Y-m-d"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_galeri->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datagaleri');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_galeri->GetDataGambarById($idb)->row();
    $LetakGambar = './files/galeri/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/galeri/';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_galeri)
  {
    $id['id_galeri'] = $this->uri->segment(3);

    $dataGbr = $this->model_galeri->GetDataGambarById($id_galeri)->row();
    $LetakGambar = './files/galeri/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_galeri->hapus($id);
      redirect('galeri/datagaleri');
    } else {
      $this->model_galeri->hapus($id);
      redirect('galeri/datagaleri');
    }
  }
}
