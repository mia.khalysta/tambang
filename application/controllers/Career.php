<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_career');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datacareer()
  {

    $data = array(
      'title' => 'Info Karir',
      'data_career' => $this->model_career->get_all(),
      'isi' => 'backend/career/data_career'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Karir',
      'data_jb' => $this->model_career->get_all(),
      'isi' => 'backend/career/tambah_career'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    //Txtcareer, TxtKet
    $data = array(
      'nama_career' => $this->input->post("Txtcareer"),
	  'keterangan' => $this->input->post("Txtket")
    );
	
	$nama_career = $this->input->post('Txtcareer');
	$sql = $this->db->query("SELECT nama_career FROM tbl_career where nama_career='$nama_career'");
	$cek_nik = $sql->num_rows();
	if ($cek_nik > 0) {
	$this->session->set_flashdata('notif', '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Peringatan! Data Karir Sudah Ada</div>');
	redirect('tambahcareer');
	}else{
    $this->model_career->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Karir Berhasil di Simpan</div>');

    redirect('datacareer');
	}
	
    
  }

  public function edit($idjb)
  {
    $idjb = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Karir',
      'data_jb' => $this->model_career->edit($idjb),
      'isi' => 'backend/career/edit_career'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id['id_career'] = $this->input->post("TxtIDcareer");
	
	$idm = $this->input->post("TxtIDcareer");
    $data = array(
      'nama_career' => $this->input->post("Txtcareer"),
	  'keterangan' => $this->input->post("Txtket")
    );

    $this->model_career->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Karir Berhasil di Update</div>');

    redirect('datacareer');
  }

  public function hapus($idj)
  {
    $id['id_career'] = $this->uri->segment(3);
    $this->model_career->hapus($id);
    redirect('datacareer');
  }

} // END OF class career
