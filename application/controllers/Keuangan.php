<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keuangan extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_keuangan');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datakeuangan()
  {

    $data = array(
      'title' => 'Laporan Keuangan',
      'data_keuangan' => $this->model_keuangan->get_all(),
      'isi' => 'backend/keuangan/data_keuangan'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Laporan Keuangan',
      'data_keuangan' => $this->model_keuangan->get_all(),
      'isi' => 'backend/keuangan/tambah_keuangan'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'judul' => $this->input->post("TxtNama"),
	  'tanggal' => date("Y-m-d"),
	  'kategori' => $this->input->post("TxtKategori"),
      'gambar' => $this->aksi_upload()
    );

    $this->model_keuangan->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Laporan Keuangan berhasil di simpan</div>');

    redirect('datakeuangan');
    //$this->datakeuangan();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/keuangan/';
    $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_keuangan)
  {
    $id_keuangan = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Laporan Keuangan',
      'data_keuangan' => $this->model_keuangan->edit($id_keuangan),
	  'kategori' => $this->input->post("TxtKategori"),
      'isi' => 'backend/keuangan/edit_keuangan'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_keuangan] = $this->input->post("id_keuangan");

    $idb = $this->input->post("id_keuangan");
    $data = array(
      'judul' => $this->input->post("TxtNama"),
	  'tanggal' => date("Y-m-d"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_keuangan->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datakeuangan');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_keuangan->GetDataGambarById($idb)->row();
    $LetakGambar = './files/keuangan/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/keuangan/';
      $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx';
      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_keuangan)
  {
    $id['id_keuangan'] = $this->uri->segment(3);

    $dataGbr = $this->model_keuangan->GetDataGambarById($id_keuangan)->row();
    $LetakGambar = './files/keuangan/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_keuangan->hapus($id);
      redirect('keuangan/datakeuangan');
    } else {
      $this->model_keuangan->hapus($id);
      redirect('keuangan/datakeuangan');
    }
  }
}
