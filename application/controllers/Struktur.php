<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Struktur extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_struktur');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datastruktur()
  {

    $data = array(
      'title' => 'Struktur Organisasi',
      'data_struktur' => $this->model_struktur->get_all(),
      'isi' => 'backend/struktur/data_struktur'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Struktur Organisasi',
      'data_struktur' => $this->model_struktur->get_all(),
      'isi' => 'backend/struktur/tambah_struktur'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'judul' => $this->input->post("TxtJudul"),
      'ket' => $this->input->post("TxtKet"),
      'gambar' => $this->aksi_upload()
    );

    $this->model_struktur->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Struktur Organisasi berhasil di simpan</div>');

    redirect('datastruktur');
    //$this->datastruktur();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/struktur/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_struktur)
  {
    $id_struktur = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Struktur Organisasi',
      'data_struktur' => $this->model_struktur->edit($id_struktur),
      'isi' => 'backend/struktur/edit_struktur'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_struktur] = $this->input->post("id_struktur");

    $idb = $this->input->post("id_struktur");
    $data = array(
      'judul' => $this->input->post("TxtJudul"),
      'ket' => $this->input->post("TxtKet"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_struktur->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datastruktur');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_struktur->GetDataGambarById($idb)->row();
    $LetakGambar = './files/struktur/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/struktur/';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_struktur)
  {
    $id['id_struktur'] = $this->uri->segment(3);

    $dataGbr = $this->model_struktur->GetDataGambarById($id_struktur)->row();
    $LetakGambar = './files/struktur/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_struktur->hapus($id);
      redirect('struktur/datastruktur');
    } else {
      $this->model_struktur->hapus($id);
      redirect('struktur/datastruktur');
    }
  }
}
