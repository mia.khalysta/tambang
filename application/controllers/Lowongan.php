<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lowongan extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_lowongan');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datalowongan()
  {

    $data = array(
      'title' => 'Info Peluang Kerja',
      'data_lowongan' => $this->model_lowongan->get_all(),
      'isi' => 'backend/lowongan/data_lowongan'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Peluang Kerja',
      'data_jb' => $this->model_lowongan->get_all(),
      'isi' => 'backend/lowongan/tambah_lowongan'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    //Txtlowongan, TxtKet
    $data = array(
      'nama_lowongan' => $this->input->post("Txtlowongan"),
	  'keterangan' => $this->input->post("Txtket")
    );
	
	$nama_lowongan = $this->input->post('Txtlowongan');
	$sql = $this->db->query("SELECT nama_lowongan FROM tbl_lowongan where nama_lowongan='$nama_lowongan'");
	$cek_nik = $sql->num_rows();
	if ($cek_nik > 0) {
	$this->session->set_flashdata('notif', '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Peringatan! Data Peluang Kerja Sudah Ada</div>');
	redirect('tambahlowongan');
	}else{
    $this->model_lowongan->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Peluang Kerja Berhasil di Simpan</div>');

    redirect('datalowongan');
	}
	
    
  }

  public function edit($idjb)
  {
    $idjb = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Peluang Kerja',
      'data_jb' => $this->model_lowongan->edit($idjb),
      'isi' => 'backend/lowongan/edit_lowongan'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id['id_lowongan'] = $this->input->post("TxtIDlowongan");
	
	$idm = $this->input->post("TxtIDlowongan");
    $data = array(
      'nama_lowongan' => $this->input->post("Txtlowongan"),
	  'keterangan' => $this->input->post("Txtket")
    );

    $this->model_lowongan->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Peluang Kerja Berhasil di Update</div>');

    redirect('datalowongan');
  }

  public function hapus($idj)
  {
    $id['id_lowongan'] = $this->uri->segment(3);
    $this->model_lowongan->hapus($id);
    redirect('datalowongan');
  }

} // END OF class lowongan
