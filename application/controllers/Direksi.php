<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Direksi extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_direksi');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datadireksi()
  {

    $data = array(
      'title' => 'Profil Direksi',
      'data_direksi' => $this->model_direksi->get_all(),
      'isi' => 'backend/direksi/data_direksi'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Profil Direksi',
      'data_direksi' => $this->model_direksi->get_all(),
      'isi' => 'backend/direksi/tambah_direksi'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'nama' => $this->input->post("TxtNama"),
      'jabatan' => $this->input->post("TxtJabatan"),
      'ket' => $this->input->post("TxtKet"),
      'gambar' => $this->aksi_upload()
    );

    $this->model_direksi->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Profil Direksi berhasil di simpan</div>');

    redirect('datadireksi');
    //$this->datadireksi();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/direksi/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_direksi)
  {
    $id_direksi = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Profil Direksi',
      'data_direksi' => $this->model_direksi->edit($id_direksi),
      'isi' => 'backend/direksi/edit_direksi'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_direksi] = $this->input->post("id_direksi");

    $idb = $this->input->post("id_direksi");
    $data = array(
      'nama' => $this->input->post("TxtNama"),
      'jabatan' => $this->input->post("TxtJabatan"),
      'ket' => $this->input->post("TxtKet"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_direksi->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datadireksi');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_direksi->GetDataGambarById($idb)->row();
    $LetakGambar = './files/direksi/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/direksi/';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_direksi)
  {
    $id['id_direksi'] = $this->uri->segment(3);

    $dataGbr = $this->model_direksi->GetDataGambarById($id_direksi)->row();
    $LetakGambar = './files/direksi/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_direksi->hapus($id);
      redirect('direksi/datadireksi');
    } else {
      $this->model_direksi->hapus($id);
      redirect('direksi/datadireksi');
    }
  }
}
