<?php

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('m_login');
    }

    function index()
    {
        $this->load->view('backend/login');
    }

    function dataprofil($idp)
    {
        $data = array(
            'title' => 'Data Profil',
            'hasil' => $this->m_login->edit($idp),
            'isi' => 'backend/data_profil'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }

    function tambah()
    {
        $data = array(
            'title' => 'Data Profil',
            'dataprofil' => $this->m_login->get_all(),
            'isi' => 'backend/tambah_profil'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }



    function aksi_login()
    {
        $username = $this->input->post('user');
        $password = $this->input->post('sandi');

        $where = array(
            'username' => $username,
            'password' => md5($password)
        );
        $cek = $this->m_login->cek_login("tbl_user", $where);
        if ($cek) {
            $data_session = array(
                'usernama' => $username,
                'nama' => $cek['nama'],
                'status' => "loginPATEN",
                'level' => $cek['level'],
                'idU' => $cek['id_user']
            );

            $this->session->set_userdata($data_session);

            redirect(base_url("admin"));
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Username atau Password Anda Salah</div>');
            redirect(base_url("Error404"));
        }
    }

    public function edit($idp)
    {
        $data = array(
            'title' => 'Edit Profil',
            'data_p' => $this->m_login->edit($idp),
            'isi' => 'backend/edit_profil'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }

    public function update()
    {
        $id_u = $this->input->post("id");
        $password = $this->input->post("pass");
        $nama = $this->input->post("nama");
        $data = array(
            'nama' => $this->input->post("nama"),
            'username' => $this->input->post("username"),
            'password' => md5($password)

        );


        $this->m_login->update($data, $id_u);
        $this->session->set_userdata('nama', $nama);
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data profil berhasil di update</div>');

        redirect('login/dataprofil/' . $id_u);
    }

    public function hapus($id)
    {
        $idu['id_user'] = $this->uri->segment(3);

        $this->m_login->hapusu($idu);
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di hapus</div>');
        redirect('login/allprofil');
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }


    // UNTUK SUPERADMIN
    function allprofil()
    {
        $data = array(
            'title' => 'Data Profil',
            'data_allprofil' => $this->m_login->get_all(),
            'isi' => 'backend/data_profilfull'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }

    function tambah_p()
    {
        $data = array(
            'title' => 'Data Profil',
            'dataprofil' => $this->m_login->get_all(),
            'isi' => 'backend/tambah_profil'
        );
        $this->load->view('backend/layout/wrapper', $data);
    }

    public function simpan()
    {
        $data = array(
            'nama' => htmlspecialchars($this->input->post('nama', true)),
            'username' => htmlspecialchars($this->input->post('username', true)),
            'level' => 'admin',
            'password' => md5($this->input->post('pass'))
        );

        $this->m_login->simpan($data);
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di simpan</div>');

        redirect('login/allprofil');
    }

    public function update_p()
    {
        $id_u = $this->input->post("id");
        $password = $this->input->post("pass");
        $nama = $this->input->post("nama");
        $data = array(
            'nama' => $this->input->post("nama"),
            'username' => $this->input->post("username"),
            'password' => md5($password)

        );

        $this->m_login->update($data, $id_u);
        $this->session->set_userdata('nama', $nama);

        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data profil berhasil di update</div>');

        redirect('login/allprofil');
    }
}
