<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Publikasi extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_publikasi');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datapublikasi()
  {

    $data = array(
      'title' => 'Data Publikasi',
      'data_publikasi' => $this->model_publikasi->get_all(),
      'isi' => 'backend/data_publikasi'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Data Publikasi',
      'data_publikasi' => $this->model_publikasi->get_all(),
      'isi' => 'backend/tambah_publikasi'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'judul' => $this->input->post("TxtJudul"),
      'tanggal' => $this->input->post("TxtTanggal"),
	  'ket' => $this->input->post("TxtKet"),
      'gambar' => $this->aksi_upload()
    );

    $this->model_publikasi->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Publikasi berhasil di simpan</div>');

    redirect('datapublikasi');
    //$this->datapublikasi();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/publikasi/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_publikasi)
  {
    $id_publikasi = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Data Publikasi',
      'data_publikasi' => $this->model_publikasi->edit($id_publikasi),
      'isi' => 'backend/edit_publikasi'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_publikasi] = $this->input->post("id_publikasi");

    $idb = $this->input->post("id_publikasi");
    $data = array(
      'judul' => $this->input->post("TxtJudul"),
      'tanggal' => $this->input->post("TxtTanggal"),
	  'ket' => $this->input->post("TxtKet"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_publikasi->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datapublikasi');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_publikasi->GetDataGambarById($idb)->row();
    $LetakGambar = './files/publikasi/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/publikasi/';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_publikasi)
  {
    $id['id_publikasi'] = $this->uri->segment(3);

    $dataGbr = $this->model_publikasi->GetDataGambarById($id_publikasi)->row();
    $LetakGambar = './files/publikasi/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_publikasi->hapus($id);
      redirect('publikasi/datapublikasi');
    } else {
      $this->model_publikasi->hapus($id);
      redirect('publikasi/datapublikasi');
    }
  }
}
