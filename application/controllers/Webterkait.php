<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Webterkait extends CI_Controller
{
   public function __construct()
   {
      parent::__construct();
      $this->load->model('model_webterkait');
      $this->load->helper(array('form', 'url'));
      if ($this->session->userdata('status') != "loginPATEN") {
         redirect(base_url("Error404"));
      }
   }

   public function index()
   {

      $this->load->view('index');
   }

   public function data_webterkait()
   {

      $data = array(
         'title' => 'Data Web Terkait',
         'datawebterkait' => $this->model_webterkait->get_all(),
         'isi' => 'backend/data_webterkait'
      );
      $this->load->view('backend/layout/wrapper', $data);
   }

   public function tambah()
   {
      $data = array(
         'title' => 'Tambah Data Web Terkait',
         'data_wt' => $this->model_webterkait->get_all(),
         'isi' => 'backend/tambah_webterkait'
      );
      $this->load->view('backend/layout/wrapper', $data);
   }

   public function simpan()
   {
      $data = array(
         'judul' => $this->input->post("TxtJudul"),
         'alamat_web' => $this->input->post("TxtSitus")
      );

      $this->model_webterkait->simpan($data);
      $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data dari situs terkait berhasil di simpan</div>');

      redirect('webterkait/data_webterkait');
   }

   public function edit($idtk)
   {
      $idtk = $this->uri->segment(3);

      $data = array(

         'title'     => 'Edit Data Web Terkait',
         'data_wt' => $this->model_webterkait->edit($idtk),
         'isi' => 'backend/edit_webterkait'
      );

      $this->load->view('backend/layout/wrapper', $data);
   }

   public function update()
   {
      $id['id_link'] = $this->input->post("TxtIdLink");

      $data = array(
         'judul' => $this->input->post("TxtJudul"),
         'alamat_web' => $this->input->post("TxtSitus")
      );

      $this->model_webterkait->update($data, $id);
      $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data dari situs terkait berhasil di update</div>');

      redirect('webterkait/data_webterkait');
   }

   public function hapus($idwt)
   {
      $id['id_link'] = $this->uri->segment(3);
      $this->model_webterkait->hapus($id);
      redirect('datawebterkait');
   }
} //END OF class Webterkait
