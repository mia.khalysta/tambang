<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Saham extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_saham');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datasaham()
  {

    $data = array(
      'title' => 'Data Saham',
      'data_saham' => $this->model_saham->get_all(),
      'isi' => 'backend/saham/data_saham'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Data Saham',
      'data_saham' => $this->model_saham->get_all(),
      'isi' => 'backend/saham/tambah_saham'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'judul' => $this->input->post("TxtNama"),
	  'tanggal' => date("Y-m-d"),
      'gambar' => $this->aksi_upload()
    );

    $this->model_saham->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Saham berhasil di simpan</div>');

    redirect('datasaham');
    //$this->datasaham();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/saham/';
    $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_saham)
  {
    $id_saham = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Data Saham',
      'data_saham' => $this->model_saham->edit($id_saham),
      'isi' => 'backend/saham/edit_saham'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_saham] = $this->input->post("id_saham");

    $idb = $this->input->post("id_saham");
    $data = array(
      'judul' => $this->input->post("TxtNama"),
	  'tanggal' => date("Y-m-d"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_saham->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datasaham');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_saham->GetDataGambarById($idb)->row();
    $LetakGambar = './files/saham/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/saham/';
      $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx';
      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_saham)
  {
    $id['id_saham'] = $this->uri->segment(3);

    $dataGbr = $this->model_saham->GetDataGambarById($id_saham)->row();
    $LetakGambar = './files/saham/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_saham->hapus($id);
      redirect('saham/datasaham');
    } else {
      $this->model_saham->hapus($id);
      redirect('saham/datasaham');
    }
  }
}
