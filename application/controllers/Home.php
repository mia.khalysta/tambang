<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
public function __construct(){
parent::__construct();
$this->load->model('model_baner');
$this->load->model('model_publikasi');
$this->load->model('model_about');
$this->load->model('model_struktur');
$this->load->model('model_dewan');
$this->load->model('model_direksi');
$this->load->model('model_komite');
$this->load->model('model_kelola');
$this->load->model('model_penghargaan');
$this->load->helper(array('form', 'url'));
}
 
public function index()
	{
		$data=array(
		'title'=>'PT. Bayan Resource, Tbk',
		'isi' =>'frontend/home',
		'data_baner' => $this->model_baner->get_all(),
		'berita' => $this->model_publikasi->limited(),
		);
		$this->load->view('frontend/layout/wrapper',$data); 
	}

public function dataabout()
	{
		$data = array(
		  'title' => 'Info Tentang',
		  'data_about' => $this->model_about->get_all(),
		  'isi' => 'frontend/about'
		);
		$this->load->view('frontend/layout/wrapper', $data);
	}

public function datastruktur()
	{
		$data = array(
		  'title' => 'Struktur Organisasi',
		  'data_struktur' => $this->model_struktur->get_all(),
		  'isi' => 'frontend/organisasi'
		);
		$this->load->view('frontend/layout/wrapper', $data);
	}
public function datadewan()
  {

    $data = array(
      'title' => 'Profil Komisaris',
      'data_komisaris' => $this->model_dewan->get_all(),
	  'data_direksi' => $this->model_direksi->get_all(),
	  'data_komite' => $this->model_komite->get_all(),
      'isi' => 'frontend/dewan'
    );
    $this->load->view('frontend/layout/wrapper', $data);
  }
public function datakelola()
  {

    $data = array(
      'title' => 'Data Tata Kelola',
      'data_kelola' => $this->model_kelola->get_all(),
      'isi' => 'frontend/tatakelola'
    );
    $this->load->view('frontend/layout/wrapper', $data);
  }
public function datapenghargaan()
  {
    $data = array(
      'title' => 'Data Penghargaan',
      'data_penghargaan' => $this->model_penghargaan->get_all(),
      'isi' => 'frontend/penghargaan'
    );
    $this->load->view('frontend/layout/wrapper', $data);
  }
}