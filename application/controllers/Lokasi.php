<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lokasi extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_lokasi');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datalokasi()
  {

    $data = array(
      'title' => 'Lokasi/Sumber Daya',
      'data_lokasi' => $this->model_lokasi->get_all(),
      'isi' => 'backend/lokasi/data_lokasi'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Lokasi/Sumber Daya',
      'data_lokasi' => $this->model_lokasi->get_all(),
      'isi' => 'backend/lokasi/tambah_lokasi'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'nama_lokasi' => $this->input->post("Txtlokasi"),
	  'keterangan' => $this->input->post("Txtket"),
      'gambar' => $this->aksi_upload(),
    );

    $this->model_lokasi->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Lokasi/Sumber Daya berhasil di simpan</div>');

    redirect('datalokasi');
    //$this->datalokasi();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/lokasi/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_lokasi)
  {
    $id_lokasi = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Lokasi/Sumber Daya',
      'data_lokasi' => $this->model_lokasi->edit($id_lokasi),
      'isi' => 'backend/lokasi/edit_lokasi'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_lokasi] = $this->input->post("id_lokasi");

    $idb = $this->input->post("id_lokasi");
    $data = array(
      'nama_lokasi' => $this->input->post("Txtlokasi"),
	  'keterangan' => $this->input->post("Txtket"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_lokasi->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datalokasi');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_lokasi->GetDataGambarById($idb)->row();
    $LetakGambar = './files/lokasi/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/lokasi/';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_lokasi)
  {
    $id['id_lokasi'] = $this->uri->segment(3);

    $dataGbr = $this->model_lokasi->GetDataGambarById($id_lokasi)->row();
    $LetakGambar = './files/lokasi/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_lokasi->hapus($id);
      redirect('lokasi/datalokasi');
    } else {
      $this->model_lokasi->hapus($id);
      redirect('lokasi/datalokasi');
    }
  }
}
