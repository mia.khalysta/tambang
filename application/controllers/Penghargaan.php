<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penghargaan extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_penghargaan');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {
    $this->load->view('index');
  }
   
  public function datapenghargaan()
  {
    $data = array(
      'title' => 'Data Penghargaan',
      'data_penghargaan' => $this->model_penghargaan->get_all(),
      'isi' => 'backend/penghargaan/data_penghargaan'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Data Penghargaan',
      'data_penghargaan' => $this->model_penghargaan->get_all(),
      'isi' => 'backend/penghargaan/tambah_penghargaan'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'judul' => $this->input->post("TxtJudul"),
      'tanggal' => date("Y-m-d"),
      'gambar' => $this->aksi_upload()
    );

    $this->model_penghargaan->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Penghargaan berhasil di simpan</div>');

    redirect('datapenghargaan');
    //$this->datapenghargaan();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/penghargaan/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_penghargaan)
  {
    $id_penghargaan = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Data Penghargaan',
      'data_penghargaan' => $this->model_penghargaan->edit($id_penghargaan),
      'isi' => 'backend/penghargaan/edit_penghargaan'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_penghargaan] = $this->input->post("id_penghargaan");

    $idb = $this->input->post("id_penghargaan");
    $data = array(
      'judul' => $this->input->post("TxtJudul"),
      'tanggal' => date("Y-m-d"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_penghargaan->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datapenghargaan');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_penghargaan->GetDataGambarById($idb)->row();
    $LetakGambar = './files/penghargaan/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/penghargaan/';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_penghargaan)
  {
    $id['id_penghargaan'] = $this->uri->segment(3);

    $dataGbr = $this->model_penghargaan->GetDataGambarById($id_penghargaan)->row();
    $LetakGambar = './files/penghargaan/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_penghargaan->hapus($id);
      redirect('penghargaan/datapenghargaan');
    } else {
      $this->model_penghargaan->hapus($id);
      redirect('penghargaan/datapenghargaan');
    }
  }
}
