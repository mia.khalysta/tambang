<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_about');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function dataabout()
  {

    $data = array(
      'title' => 'Info Tentang',
      'data_about' => $this->model_about->get_all(),
      'isi' => 'backend/about/data_about'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah About',
      'data_jb' => $this->model_about->get_all(),
      'isi' => 'backend/about/tambah_about'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    //Txtabout, TxtKet
    $data = array(
      'nama_about' => $this->input->post("Txtabout"),
	  'keterangan' => $this->input->post("Txtket")
    );
	
	$nama_about = $this->input->post('Txtabout');
	$sql = $this->db->query("SELECT nama_about FROM tbl_about where nama_about='$nama_about'");
	$cek_nik = $sql->num_rows();
	if ($cek_nik > 0) {
	$this->session->set_flashdata('notif', '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Peringatan! Info Tentang Sudah Ada</div>');
	redirect('tambahabout');
	}else{
    $this->model_about->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Info Tentang Berhasil di Simpan</div>');

    redirect('dataabout');
	}
	
    
  }

  public function edit($idjb)
  {
    $idjb = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit About',
      'data_jb' => $this->model_about->edit($idjb),
      'isi' => 'backend/about/edit_about'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id['id_about'] = $this->input->post("TxtIDabout");
	
	$idm = $this->input->post("TxtIDabout");
    $data = array(
      'nama_about' => $this->input->post("Txtabout"),
	  'keterangan' => $this->input->post("Txtket")
    );

    $this->model_about->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Info Tentang Berhasil di Update</div>');

    redirect('dataabout');
  }

  public function hapus($idj)
  {
    $id['id_about'] = $this->uri->segment(3);
    $this->model_about->hapus($id);
    redirect('dataabout');
  }

} // END OF class about
