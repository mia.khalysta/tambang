<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelola extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_kelola');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datakelola()
  {

    $data = array(
      'title' => 'Data Tata Kelola',
      'data_kelola' => $this->model_kelola->get_all(),
      'isi' => 'backend/kelola/data_kelola'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Data Tata Kelola',
      'data_kelola' => $this->model_kelola->get_all(),
      'isi' => 'backend/kelola/tambah_kelola'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'judul' => $this->input->post("TxtNama"),
      'ket' => $this->input->post("TxtKet"),
      'gambar' => $this->aksi_upload()
    );

    $this->model_kelola->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Tata Kelola berhasil di simpan</div>');

    redirect('datakelola');
    //$this->datakelola();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/kelola/';
    $config['allowed_types']        = 'jpg|png|jpeg|doc|docx|pdf';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_kelola)
  {
    $id_kelola = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Data Tata Kelola',
      'data_kelola' => $this->model_kelola->edit($id_kelola),
      'isi' => 'backend/kelola/edit_kelola'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_kelola] = $this->input->post("id_kelola");

    $idb = $this->input->post("id_kelola");
    $data = array(
      'judul' => $this->input->post("TxtNama"),
      'ket' => $this->input->post("TxtKet"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_kelola->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datakelola');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_kelola->GetDataGambarById($idb)->row();
    $LetakGambar = './files/kelola/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/kelola/';
      $config['allowed_types']        = 'jpg|png|jpeg|doc|docx|pdf';

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_kelola)
  {
    $id['id_kelola'] = $this->uri->segment(3);

    $dataGbr = $this->model_kelola->GetDataGambarById($id_kelola)->row();
    $LetakGambar = './files/kelola/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_kelola->hapus($id);
      redirect('kelola/datakelola');
    } else {
      $this->model_kelola->hapus($id);
      redirect('kelola/datakelola');
    }
  }
}
