<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rilis extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_rilis');
    $this->load->helper(array('form', 'url'));
    if ($this->session->userdata('status') != "loginPATEN") {
      redirect(base_url("Error404"));
    }
  }

  public function index()
  {

    $this->load->view('index');
  }

  public function datarilis()
  {

    $data = array(
      'title' => 'Rilis Berita',
      'data_rilis' => $this->model_rilis->get_all(),
      'isi' => 'backend/rilis/data_rilis'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function tambah()
  {
    $data = array(
      'title' => 'Tambah Rilis Berita',
      'data_rilis' => $this->model_rilis->get_all(),
      'isi' => 'backend/rilis/tambah_rilis'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }

  public function simpan()
  {
    $data = array(
      'judul' => $this->input->post("TxtNama"),
	  'tanggal' => date("Y-m-d"),
      'gambar' => $this->aksi_upload()
    );

    $this->model_rilis->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Rilis Berita berhasil di simpan</div>');

    redirect('datarilis');
    //$this->datarilis();
  }

  private function aksi_upload()
  {
    //$NmGbrbru = 'item-' . date('ymd') . '-' . substr(md5(rand()), 0, 10);
    $config['upload_path']          = './files/rilis/';
    $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx|gif|jpg|png|jpeg';
    //$config['max_size']             = 2048;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('berkas')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $data = $this->upload->data();
      //return $NmGbrbru;
      return $data['file_name'];
    }
  }

  public function edit($id_rilis)
  {
    $id_rilis = $this->uri->segment(3);

    $data = array(

      'title'     => 'Edit Rilis Berita',
      'data_rilis' => $this->model_rilis->edit($id_rilis),
      'isi' => 'backend/rilis/edit_rilis'
    );

    $this->load->view('backend/layout/wrapper', $data);
  }

  public function update()
  {
    $id[id_rilis] = $this->input->post("id_rilis");

    $idb = $this->input->post("id_rilis");
    $data = array(
      'judul' => $this->input->post("TxtNama"),
	  'tanggal' => date("Y-m-d"),
      'gambar' => !empty($_FILES["berkas"]["name"]) != "" ? $this->UpdateGambarB($idb) : $this->input->post("berkas_lama"),
    );

    $this->model_rilis->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! data berhasil di update</div>');

    redirect('datarilis');
  }

  private function UpdateGambarB($idb)
  {
    $dataGbr = $this->model_rilis->GetDataGambarById($idb)->row();
    $LetakGambar = './files/rilis/' . $dataGbr->gambar;

    if ((is_readable($LetakGambar)) && (unlink($LetakGambar))) {
      $config['upload_path']          = './files/rilis/';
      $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx|gif|jpg|png|jpeg';
      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('berkas')) {
        $error = array('error' => $this->upload->display_errors());
      } else {
        $data = $this->upload->data();
        //return $NmGbrbru;
        return $data['file_name'];
      }
    } else {
      echo 'Cek kembali ';
    }
  }

  public function hapus($id_rilis)
  {
    $id['id_rilis'] = $this->uri->segment(3);

    $dataGbr = $this->model_rilis->GetDataGambarById($id_rilis)->row();
    $LetakGambar = './files/rilis/' . $dataGbr->gambar;

    if (is_readable($LetakGambar) && unlink($LetakGambar)) {
      $this->model_rilis->hapus($id);
      redirect('rilis/datarilis');
    } else {
      $this->model_rilis->hapus($id);
      redirect('rilis/datarilis');
    }
  }
}
