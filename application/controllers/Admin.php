<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //$this->load->model('model_merek');
        //$this->load->model('model_baner');
        $this->load->helper(array('form', 'url'));
        if ($this->session->userdata('status') != "loginPATEN") {
            redirect(base_url("Error404"));
        }
    }

    public function index()
    {
        $data = array(
            'title' => 'Admin Informasi Publik',
            'isi' => 'backend/home',
            //'data_merek' => $this->model_merek->get_all(),
            //'data_baner' => $this->model_baner->get_all()
        );
        $this->load->view('backend/layout/wrapper', $data);
    }
}
